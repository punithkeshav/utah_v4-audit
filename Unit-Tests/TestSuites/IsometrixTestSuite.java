/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.currentDevice;
import static KeywordDrivenTestFramework.Core.BaseClass.currentDeviceConfig;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author MJivan
 */
public class IsometrixTestSuite extends BaseClass
{

    static TestMarshall instance;
    public static Enums.DeviceConfig test;

    public IsometrixTestSuite()
    {
         appConfig = new ApplicationConfig();
        TestMarshall.currentEnvironment = Enums.Environment.QA;

        //*******************************************
    }

    @Test
    public void IsometrixTestSuite() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - Scenario 1  - Test Pack");
        instance = new TestMarshall("TestPacks\\FR26-Capture Incident Management Findings.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    
    @Test
    public void IsometrixIncidentManagment() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - Scenario 1  - Test Pack");
        instance = new TestMarshall("TestPacks\\UC_RSM_01_02_CaptureRiskRegisterfromexistingMainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    @Test
    public void IsoMetrixRiskManagment() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - Scenario 1  - Test Pack");
        instance = new TestMarshall("TestPacks\\IsometrixRiskManagement.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    @Test
    public void InjuredPersons() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - Scenario 1  - Test Pack");
        instance = new TestMarshall("TestPacks\\InjuredPersons.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    @Test
    public void Environment() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - Scenario 1  - Test Pack");
        instance = new TestMarshall("TestPacks\\Environment.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    
    @Test
    public void MobileTest() throws FileNotFoundException
    {
        Narrator.logDebug("Isometrix - Scenario 1  - Test Pack");
        currentDevice = Enums.Device.Emulator;
        currentDeviceConfig = Enums.DeviceConfig.Test;
        
        instance = new TestMarshall("TestPacks\\IsometrixPOC.xlsx.xlsx", Enums.BrowserType.mobileChrome);
        instance.runKeywordDrivenTests();
    }

}

