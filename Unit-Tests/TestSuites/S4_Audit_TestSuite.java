/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author RNagel
 */
public class S4_Audit_TestSuite extends BaseClass {

    static TestMarshall instance;

    public S4_Audit_TestSuite() {
        ApplicationConfig appConfig = new ApplicationConfig();
        TestMarshall.currentEnvironment = Enums.Environment.MTN;

        //*******************************************
    }

    //S4.0 Audit full Execution
    @Test
    public void S4_Audit_Beta() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\S4_Audit_Beta.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }    
    
    //FR1-Capture_Audit - Main_Scenario
    @Test
    public void FR1_CaptureAction_MainSc1enario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Audit\\FR1-Capture_Audit - Main_Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    @Test
    public void FR1_CaptureAudit_AlternateScenario1() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Audit\\FR1-Capture_Audit - AlternateScenario1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    @Test 
    public void FR1_CaptureAudit_AlternativeScenario2() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Audit\\FR1-Capture_Audit - AlternativeScenario2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    @Test
    public void FR1_CaptureAudit_AlternativeScenario3() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Audit\\FR1-Capture_Audit - AlternativeScenario3.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //UC_AUR_01_02UpdateAuditMainScenario
    @Test
    public void UC_AUR_01_02UpdateAudit_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Audit\\UC_AUR_01_02UpdateAuditMainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    @Test
    public void UC_AUR_01_02UpdateAudit_AlternateScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Audit\\UC_AUR_01_02UpdateAuditAlternateScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    @Test 
    public void UC_AUR_01_2UpdateAuditOptionalScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Audit\\UC_AUR_01_2UpdateAuditOptionalScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //UC_AUR_01_03_Audit_Manager_Updates_Audit - Main_Scenario
    @Test
    public void UC_AUR_01_03_Audit_Manager_Updates_Audit_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Audit\\UC_AUR_01_03_Audit_Manager_Updates_Audit - Main_Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    @Test
    public void UC_AUR_01_03_Audit_Manager_Updates_Audit_AlternateScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Audit\\UC_AUR_01_03_Audit_Manager_Updates_Audit - Alternate_Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR2-Capture_Audit_Team - Main_Scenario
    @Test
    public void FR2_CaptureAuditTeam_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Audit\\FR2-Capture_Audit_Team - Main_Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR3-Start_Audit_MainScenario
    @Test
    public void FR3_StartAudit_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Audit\\FR3-Start_Audit_MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR4-Capture_Audit_Findings - Main Scenario
    @Test
    public void FR4_CaptureAuditFindings_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Audit\\FR4-Capture_Audit_Findings - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR5-View_Related_Risks - Main_Scenario
    @Test
    public void FR5_ViewRelatedRisks_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Audit\\FR5-View_Related_Risks - Main_Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR6-View_Related_Obligations - MainScenario
    @Test
    public void FR6_ViewRelatedObligations_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Audit\\FR6-View_Related_Obligations - MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR7-View Related Incidents - Main Scenario
    @Test
    public void FR7_ViewRelatedIncidents_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Audit\\FR7-View Related Incidents - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR8- Audit automatically goes overdue 
    @Test
    public void FR8_AuditAutomaticallyGoesOverdue_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Audit\\FR8- Audit automatically goes overdue - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR9-Capture_Audit_Actions - Main Scenario
    @Test
    public void FR9_CaptureAuditActions_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Audit\\FR9-Capture_Audit_Actions - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR10-Incident Audit is Awaiting Sign Off - Main Scenario
    @Test
    public void FR10_IncidentAuditIsAwaitingSignOff_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Audit\\FR9-Capture_Audit_Actions - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR11-Sign Off Approval - MainScenario
    @Test
    public void FR11_SignOffApproval_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Audit\\FR11-Sign Off Approval - MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    @Test
    public void FR11_SignOffApproval_AlternateScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Audit\\FR11-Sign Off Approval - AlternateScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR12 - View_and_Print_Report - Main Scenario
    @Test
    public void FR12_ViewAndPrintReport_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Audit\\FR12 - View_and_Print_Report - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR13-View Audit Registers - Main Scenario
    @Test
    public void FR13_ViewAuditRegisters_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Audit\\FR13-View Audit Registers - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR14–View Dashboards - Main Scenario
    @Test
    public void FR14_ViewDashboards_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Audit\\FR14–View Dashboards - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
}
