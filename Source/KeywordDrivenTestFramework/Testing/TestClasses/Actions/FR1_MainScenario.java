/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Actions;

import KeywordDrivenTestFramework.Testing.TestClasses.Audit_Management.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Audit_Management_PageObjects.Audit_Management_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SMABE
 */

@KeywordAnnotation(
        Keyword = "",
        createNewBrowserInstance = false
)

public class FR1_MainScenario extends BaseClass
{
     String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR1_MainScenario()
    {
    }
    
      public TestResult executeTest()
    {

//        if (!ScheduleApproved())
//        {
//            return narrator.testFailed("Failed to capture audit details due to : - " + error);
//        }
        return narrator.finalizeTest("Successfully ");
    }
    
}
