/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Audit_Management;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Audit_Management_PageObjects.Audit_Management_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SMABE
 */
@KeywordAnnotation(
        Keyword = "UC AUC 02-02 Try approve the audit schedule",
        createNewBrowserInstance = false
)
public class UC_AUC_02_02_MainScenario extends BaseClass
{
// 

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public UC_AUC_02_02_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {

        if (!ScheduleApproved())
        {
            return narrator.testFailed("Failed to capture audit details due to : - " + error);
        }
        return narrator.finalizeTest("Successfully Try and approve the audit schedule as a user other than the Audit Manager");
    }

    public boolean ScheduleApproved()
    {

        if (!SeleniumDriverInstance.scrollToElement(Audit_Management_PageObjects.AudiEndDate()))
        {
            error = "Failed to scroll to audit end date";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.ScheduleApprovedCheckBox()))
        {
            error = "Failed to wait for Schedule Approved CheckBox.";
            return false;
        }
        
        if (SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.ScheduleApprovedCheckBox()))
        {
            narrator.stepPassedWithScreenShot("Schedule approved check box is no clickable");
            
        } else
        {
            error = "Schedule approved checkbox is clickable";
            return false;
        }

        //Getting the action No
         //Getting the action No
            String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Audit_Management_PageObjects.getActionRecord());
            String[] record = acionRecord.split(" ");
            Audit_Management_PageObjects.setRecord_Number(record[2]);
            String record_ = Audit_Management_PageObjects.getRecord_Number();

            narrator.stepPassed("Record number :" + acionRecord);
            narrator.stepPassedWithScreenShot("Successfully Propose new audit dates as a user other than the Audit Manager ");

        return true;

    }

}
