/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Audit_Management;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Audit_Management_PageObjects.Audit_Management_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SMABE
 */


@KeywordAnnotation(
        Keyword = "UC_AUC_02_04_MainScenario",
        createNewBrowserInstance = false
)
public class UC_AUC_02_04_MainScenario  extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public UC_AUC_02_04_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {
     

        if (!ScheduleApproved())
        {
            return narrator.testFailed("Failed to capture audit details due to : - " + error);
        }
        return narrator.finalizeTest("Successfully Propose new audit dates as the Audit Manager");
    }
    
    
      
       public boolean ScheduleApproved()
    {

        if (!SeleniumDriverInstance.scrollToElement(Audit_Management_PageObjects.AudiEndDate()))
        {
            error = "Failed to scroll to audit end date";
            return false;

        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.ProposeNewDatesCheckBox()))
        {
            error = "Failed to wait for Propose new dates CheckBox.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.ProposeNewDatesCheckBox()))
        {
            error = "Failed to click on 'Propose new dates' CheckBox";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully Clicked Propose new dates  check box");

        //Suggested start date
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.SuggestedStartDate()))
        {
            error = "Failed to wait for  Suggested start date textarea.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Audit_Management_PageObjects.SuggestedStartDate(), startDate))
        {
            error = "Failed to enter '" + startDate + "' into Suggested start Date textarea.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Suggested start date : '" + endDate + "'.");

        //Suggested end Date 
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.SuggestedEndDate()))
        {
            error = "Failed to wait for Suggested start date textarea.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Audit_Management_PageObjects.SuggestedEndDate(), endDate))
        {
            error = "Failed to enter '" + endDate + "' into Suggested end Date textarea.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Suggested end date : '" + endDate + "'.");

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.NewProposedDatesComments()))
        {
            error = "Failed to wait for Comments  textarea.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Audit_Management_PageObjects.NewProposedDatesComments(), getData("Comments")))
        {
            error = "Failed to enter '" + getData("Comments") + "' into Comments  textarea.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Comments  : '" + getData("Comments") + "'.");

       
        return true;

    }

    
}
