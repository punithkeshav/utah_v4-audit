/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Audit_Management;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Audit_Management_PageObjects.Audit_Management_PageObjects;
import KeywordDrivenTestFramework.Testing.PageObjects.Audit_Management_PageObjects.MailSlurper_PageObjects;

import microsoft.exchange.webservices.data.property.complex.availability.Suggestion;

/**
 *
 * @author MJivan
 */
@KeywordAnnotation(
        Keyword = "Sign In To MailSlurper v5 - Actions",
        createNewBrowserInstance = false
)
public class MailSlurperSignIn extends BaseClass
{

    String parentWindow;
    String error = "";

    public MailSlurperSignIn()
    {

    }

    public TestResult executeTest()
    {

        if (!NavigateToMailSlurperSignInPage())
        {
            return narrator.testFailed("Failed to navigate to MailSlurper Sign In Page due to :" + error);
        }

        // This step will sign into the specified gmail account with the provided credentials
        if (!SignInToMailSlurper())
        {
            return narrator.testFailed("Failed to sign into the MailSlurper Home Page due to :" + error);
        }
        //This step will click the newly added record
        if (!clickEmailLink())
        {
            return narrator.testFailed("Failed to click record link due to :" + error);
        }

        return narrator.finalizeTest("Successfully Navigated through the Isometrix web page");
    }

    public boolean NavigateToMailSlurperSignInPage()
    {

        if (!SeleniumDriverInstance.OpenNewTab())
        {
            return false;
        }

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch to new tab";
            return false;
        }
        if (!SeleniumDriverInstance.navigateTo(getData("URL")))
        {
            error = "Failed to navigate to Isometrix Home Page.";
            return false;
        }

        return true;
    }

    public boolean SignInToMailSlurper()
    {
        if (!SeleniumDriverInstance.enterTextByXpath(MailSlurper_PageObjects.Username(), testData.getData("Username")))
        {
            error = "Failed to enter text into email text field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully Added the Username: " + testData.getData("Username") + " to the Username Text Field");

        if (!SeleniumDriverInstance.enterTextByXpath(MailSlurper_PageObjects.Password(), testData.getData("Password")))
        {
            error = "Failed to enter text into email text field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully Added the Password: " + testData.getData("Password") + " to the Password Text Field");

        if (!SeleniumDriverInstance.clickElementbyXpath(MailSlurper_PageObjects.LoginBtn()))
        {
            error = "Failed to click sign in button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully Clicked The Sign In Button");
        pause(5000);

        return true;
    }

    public boolean clickEmailLink()
    {

        String record = Audit_Management_PageObjects.getRecord_Number();
        parentWindow = SeleniumDriverInstance.Driver.getWindowHandle();

        //Search button
        if (!SeleniumDriverInstance.waitForElementByXpath(MailSlurper_PageObjects.searchBtn()))
        {
            error = "Failed to wait for Search button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(MailSlurper_PageObjects.searchBtn()))
        {
            error = "Failed to click on Search button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully Clicked Search button");

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch to window";
            return false;

        }

        if (!SeleniumDriverInstance.enterTextByXpath(MailSlurper_PageObjects.SubjectOrMessage(), record))
        {
            error = "Failed to enter text into Subject or Message text field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered the Record number: " + record);

        //Search button
        if (!SeleniumDriverInstance.waitForElementByXpath(MailSlurper_PageObjects.search_Btn()))
        {
            error = "Failed to wait for Search button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(MailSlurper_PageObjects.search_Btn()))
        {
            error = "Failed to click on Search button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully Clicked Search button");

        pause(3000);
        //SeleniumDriverInstance.Driver.switchTo().window(parentWindow);
        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch to window";
            return false;

        }

        String Record_Number = Audit_Management_PageObjects.getRecord_Number();
        String[] number = Record_Number.split("#");

        pause(3000);

        //Click the newly added record
        if (!SeleniumDriverInstance.waitForElementByXpath(MailSlurper_PageObjects.recordLink(number[1])))
        {
            error = "Failed to wait for the record link." + number[1];
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(MailSlurper_PageObjects.recordLink(number[1])))
        {
            error = "Failed to click on the record link." + number[1];
            return false;
        }
        //Link back to record
        if (!SeleniumDriverInstance.waitForElementByXpath(MailSlurper_PageObjects.linkBackToRecord()))
        {
            error = "Failed to wait Link back to record.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Audit Management record has been added");

        if (!SeleniumDriverInstance.clickElementbyXpath(MailSlurper_PageObjects.linkBackToRecord()))
        {
            error = "Failed to click on Link back to record.";
            return false;
        }

        pause(5000);
        if (!SeleniumDriverInstance.switchToFrameByXpath(Audit_Management_PageObjects.iframeXpath()))
        {
            error = "Failed to wait switch to i frame";
            return false;

        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.AuditDetailsTab()))
        {
            error = "Failed to wait for audit details tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully Clicked The Record Link: " + Record_Number);
        pause(5000);

        String window2=SeleniumDriverInstance.Driver.getWindowHandle();
        String window = Audit_Management_PageObjects.getWindow();

          
        return true;

    }

}
