/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Audit_Management;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Audit_Management_PageObjects.Audit_Management_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SMABE
 */
@KeywordAnnotation(
        Keyword = "UC_AUC_02_05_AlternateScenario",
        createNewBrowserInstance = false
)
public class UC_AUC_02_05_AkternateScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public UC_AUC_02_05_AkternateScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {

        if (!ScheduleApproved())
        {
            return narrator.testFailed("Failed to capture audit details due to : - " + error);
        }
        return narrator.finalizeTest("Successfully Audit manager accepts new proposed dates");
    }

    public boolean ScheduleApproved()
    {

//        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.AcceptProposedDatesCheckBox()))
//        {
//            error = "Failed to wait for Accept proposed dates CheckBox.";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.AcceptProposedDatesCheckBox()))
//        {
//            error = "Failed to click on Accept proposed dates CheckBox";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Successfully clicked Accept proposed dates check box");

        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.ButtonSave()))
        {
            error = "Failed to wait for Save button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.ButtonSave()))
        {
            error = "Failed to click Save button.";
            return false;
        }

        //Check if the record has been Saved
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }

        String saved = SeleniumDriverInstance.retrieveTextByXpath(Audit_Management_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Audit_Management_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }
        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Audit_Management_PageObjects.getActionRecord());
        narrator.stepPassed("Record number :" + acionRecord);

        if (getData("Submit new dates").equalsIgnoreCase("True"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.SubmitNewDatesButton()))
            {
                error = "Failed to wait for Submit new dates button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.SubmitNewDatesButton()))
            {
                error = "Failed to click on Submit new dates button";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully clicked Submit new dates button");

        }

        return true;

    }

}
