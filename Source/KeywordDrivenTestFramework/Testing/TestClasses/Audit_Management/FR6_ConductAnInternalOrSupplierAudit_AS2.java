/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Audit_Management;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Audit_Management_PageObjects.Audit_Management_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author SMABE
 */
@KeywordAnnotation(
        Keyword = "FR6 Conduct an internal or supplier  - Alternate Scenario 2",
        createNewBrowserInstance = false
)

public class FR6_ConductAnInternalOrSupplierAudit_AS2 extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR6_ConductAnInternalOrSupplierAudit_AS2()
    {
    }

    public TestResult executeTest()
    {

        if (!NavigateToFinish())
        {
            return narrator.testFailed("Failed To Navigate To Finish Due To : - " + error);
        }

        return narrator.finalizeTest("Successfully Navigated To Finish");
    }

    public boolean NavigateToFinish()
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.CloseRecord_4()))
        {
            error = "Failed to wait for the cross";
            return false;

        }

        List<WebElement> Header = SeleniumDriverInstance.Driver.findElements(By.xpath(Audit_Management_PageObjects.TabsTotal()));

        int val_1 = 1, val_2 = 0;
        val_1 = Header.size();
        val_2 = val_1 - 1;
        for (int i = 0; i < val_2; i++)
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.NextButton()))
            {
                error = "Failed to wait for the Next Button";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.NextButton()))
            {
                error = "Failed to click the  Next Button";
                return false;

            }

        }

        while (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.FinishButton()))
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.NextButton()))
            {
                error = "Failed to click the  Next Button";
                return false;

            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.FinishButton()))
        {
            error = "Failed to wait for the Finish Button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.FinishButton()))
        {
            error = "Failed to click the  Finish Button";
            return false;
        }
        
        pause(10000);

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.processFlow()))
        {
            error = "Failed to wait for process flow.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully Clicked Finish Button");

        return true;
    }
}
