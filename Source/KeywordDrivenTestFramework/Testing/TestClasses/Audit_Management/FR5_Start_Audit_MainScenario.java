/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Audit_Management;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Audit_Management_PageObjects.Audit_Management_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.openqa.selenium.JavascriptExecutor;

/**
 *
 * @author SMABE
 */
@KeywordAnnotation(
        Keyword = "FR5 Start Audit Main Scenario",
        createNewBrowserInstance = false
)

public class FR5_Start_Audit_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR5_Start_Audit_MainScenario()
    {
    }

    public TestResult executeTest()
    {

        if (!StartAudit())
        {
            return narrator.testFailed("Start Audit Due To : " + error);
        }

        return narrator.finalizeTest("Successfully Start Audit");
    }

    public boolean StartAudit()
    {
        
        
        
        if (!SeleniumDriverInstance.PageUp())
        {
            error = "Failed to scroll up";
            return false;
        }
        if (!SeleniumDriverInstance.PageUp())
        {
            error = "Failed to scroll up";
            return false;
        }
        
        if (!SeleniumDriverInstance.scrollToElement(Audit_Management_PageObjects.AuditDetailsTab()))
        {
            error = "Failed to scroll to element";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.AuditDetailsTab()))
        {
            error = "Failed to wait for audit details tab";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully waited for the start audit button");
        
         
          if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.StartAuditButton()))
        {
            error = "Failed to wait for start audit button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.StartAuditButton()))
        {
            error = "Failed to click start audit button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked starte audit");
        
          //Person conducting the audit
        if (SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.PersonConductingTheAuditDropDown()))
        {

            if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.PersonConductingTheAuditDropDown()))
            {
                error = "Failed to click  Person conducting the audit drop down.";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.TypeSearch()))
            {
                error = "Failed to wait for Person conducting the audittext box.";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(Audit_Management_PageObjects.TypeSearch(), getData("Auditee")))
            {
                error = "Failed to enter Person conducting the audit";
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter())
            {
                error = "Failed to press enter";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.AudiTemplateType(getData("Auditee"))))
            {
                error = "Failed to wait for Person conducting the audit drop down option : " + getData("Auditee");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.AudiTemplateType(getData("Auditee"))))
            {
                error = "Failed to click Person conducting the audit drop down option : " + getData("Auditee");
                return false;
            }

        }
        
         //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.ButtonSave()))
        {
            error = "Failed to wait for Save button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.ButtonSave()))
        {
            error = "Failed to click Save button.";
            return false;
        }
        
   
        //Check if the record has been Saved
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }

        String saved = SeleniumDriverInstance.retrieveTextByXpath(Audit_Management_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");

            //Getting the action No
            String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Audit_Management_PageObjects.getActionRecord());
            String[] record = acionRecord.split(" ");
            Audit_Management_PageObjects.setRecord_Number(record[2]);
            String record_ = Audit_Management_PageObjects.getRecord_Number();
            narrator.stepPassed("Record number :" + acionRecord);

        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Audit_Management_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        narrator.stepPassedWithScreenShot("Audit Started");
        return true;
    }

    public boolean RisksAndEvents()
    {

        //Audit Team
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.EventsTab()))
        {
            error = "Failed to wait for Events Tab.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.EventsTab()))
        {
            error = "Failed to click for Events Tab.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully Clicked Events Tab");

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.EventsRecord()))
        {
            error = "Failed to wait for Events Record.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.EventsRecord()))
        {
            error = "Failed to click for Events Record.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully Clicked Events Record");

        pause(20000);

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch to record tab";
            return false;
        }
        pause(5000);

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch to record tab";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully Switched To Events Record Click");

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.iframeXpath()))
        {
            error = "Failed to switch to frame.";
            return false;

        }

        if (!SeleniumDriverInstance.switchToFrameByXpath(Audit_Management_PageObjects.iframeXpath()))
        {
            error = "Failed to switch to frame";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.CloseRecord_2()))
        {
            error = "Failed to wait for the cross to click cross";
            return false;

        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.CloseRecord_2()))
        {
            error = "Failed to click cross";
            return false;

        }

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch tab.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.ButtonConfirm()))
        {
            error = "Failed to wait for the yes button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.ButtonConfirm()))
        {
            error = "Failed to click the yes button";
            return false;
        }

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch tab.";
            return false;
        }

        if (!SeleniumDriverInstance.switchToFrameByXpath(Audit_Management_PageObjects.iframeXpath()))
        {
            error = "Failed to switch to frame";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.EventManagementHeading()))
        {
            error = "Failed to wait for Event Management heading";
            return false;
        }

        narrator.stepPassedWithScreenShot("Event Management Navigated");

        pause(5000);

        return true;

    }

}
