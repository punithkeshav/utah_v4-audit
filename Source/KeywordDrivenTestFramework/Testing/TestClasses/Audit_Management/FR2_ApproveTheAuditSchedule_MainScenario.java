/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Audit_Management;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Audit_Management_PageObjects.Audit_Management_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SMABE
 */
@KeywordAnnotation(
        Keyword = "FR2 Approve the audit - Main Scenario",
        createNewBrowserInstance = false
)
public class FR2_ApproveTheAuditSchedule_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR2_ApproveTheAuditSchedule_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {

        if (!ScheduleApproved())
        {
            return narrator.testFailed("Schedule Approved Failed Due To : - " + error);
        }
        return narrator.finalizeTest("Successfully Approved the audit schedule or propose new dates for the audit");
    }

    

    public boolean ScheduleApproved()
    {

        if (!SeleniumDriverInstance.scrollToElement(Audit_Management_PageObjects.AudiEndDate()))
        {
            error = "Failed to scroll to audit end date";
            return false;

        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.ScheduleApprovedCheckBox()))
        {
            error = "Failed to wait for Schedule Approved CheckBox.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.ScheduleApprovedCheckBox()))
        {
            error = "Failed to click on 'Schedule Approved CheckBox";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked schedule approved check box");

        //Submit audit planning
        if (getData("Submit audit planning").equalsIgnoreCase("true"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.SubmitAuditPlanningButton()))
            {
                error = "Failed to wait for Submit audit planning button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.SubmitAuditPlanningButton()))
            {
                error = "Failed to click on Submit audit planning button.";
                return false;
            }

            narrator.stepPassedWithScreenShot("Successfully clicked Submit audit planning button");

            if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.RecordLabel()))
            {
                error = "Failed to wait for  Audit Management label";
                return false;
            }
            
             String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Audit_Management_PageObjects.getActionRecord());
            narrator.stepPassed("Record number :" + acionRecord);
           
            narrator.stepPassedWithScreenShot("Successfully Submitted Audit");
        } else
        {

            //Save button
            if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.AuditButtonSave()))
            {
                error = "Failed to wait for Save button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.AuditButtonSave()))
            {
                error = "Failed to click Save button.";
                return false;
            }

            pause(4000);
            //Check if the record has been Saved
            if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }

            String saved = SeleniumDriverInstance.retrieveTextByXpath(Audit_Management_PageObjects.recordSaved_popup());

            if (saved.equals("Record saved"))
            {
                narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");

                //Getting the action No
                String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Audit_Management_PageObjects.getActionRecord());
                String[] record = acionRecord.split(" ");
                Audit_Management_PageObjects.setRecord_Number(record[2]);
                String record_ = Audit_Management_PageObjects.getRecord_Number();

                narrator.stepPassed("Record number :" + acionRecord);
                narrator.stepPassedWithScreenShot("Successfully captured audit");
            } else
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.failed()))
                {
                    error = "Failed to wait for error message.";
                    return false;
                }

                String failed = SeleniumDriverInstance.retrieveTextByXpath(Audit_Management_PageObjects.failed());

                if (failed.equals("ERROR: Record could not be saved"))
                {
                    error = "Failed to save record.";
                    return false;
                }
            }

            // business unit 
            if (!SeleniumDriverInstance.scrollToElement(Audit_Management_PageObjects.BusinessUnitDropDown()))
            {
                error = "Failed to scroll to bussiness unit";
                return false;
            }
            
        }

        return true;
    }

}
