/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Audit_Management;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Audit_Management_PageObjects.Audit_Management_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SMABE
 */
@KeywordAnnotation(
        Keyword = "Add Audit Management",
        createNewBrowserInstance = false
)
public class FR1_Add_Audit_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR1_Add_Audit_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!Audit_Management())
        {
            return narrator.testFailed("Failed to navigate to Audit Management due to :  " + error);
        }

        if (!CaptureAuditDetails())
        {
            return narrator.testFailed("Failed to capture audit details due to : " + error);
        }
        return narrator.finalizeTest("Successfully navigate to Audit Management");
    }

    public boolean Audit_Management()
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.AuditManagementLabel()))
        {
            error = "Failed to wait for  Audit Management label";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully navigated to Audit Management ");

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.AuditManagementLabel()))
        {
            error = "Failed to click Audit Management label";
            return false;
        }

        pause(3000);
        //Add Button
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.Actions_add()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.Actions_add()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Audit_Management");

        return true;

    }

    public boolean CaptureAuditDetails()
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.processFlow()))
        {
            error = "Failed to wait for process flow.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.processFlow()))
        {
            error = "Failed to click process flow.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Process flow.");

        // business unit 
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.BusinessUnitDropDown()))
        {
            error = "Failed to wait for business unit drop down.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.BusinessUnitDropDown()))
        {
            error = "Failed to click business unit drop down.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for Business Unit text box.";
            return false;
        }

        pause(2000);

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.BusinessUnitexpandButton()))
        {
            error = "Failed to wait to expand Business Unit";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.BusinessUnitexpandButton()))
        {
            error = "Failed to expand Business Unit";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to wait for Business Unit:" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to click Business Unit Option drop down :" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            error = "Failed to wait for Business Unit Option drop down :" + getData("Business unit 2");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            error = "Failed to click  Business Unit Option drop down :" + getData("Business unit 2");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.Text3(getData("Business unit option"))))
        {
            error = "Failed to wait for Business Unit drop down option : " + getData("Business unit option");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.Text3(getData("Business unit option"))))
        {
            error = "Failed to click Business Unit drop down option : " + getData("Business unit option");
            return false;
        }

        narrator.stepPassedWithScreenShot("Business unit option  :" + getData("Business unit option"));

        //Functional location
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.FunctionalLocationDropDown()))
        {
            error = "Failed to wait for Functional location drop down.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.FunctionalLocationDropDown()))
        {
            error = "Failed to click Functional location drop down.";
            return false;
        }

         if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.TypeSearch2()))
            {
                error = "Failed to wait for Function Location text box.";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(Audit_Management_PageObjects.TypeSearch2(), getData("Function Location")))
            {
                error = "Failed to enter Function Location option :" + getData("Function Location");
                return false;
            }
            if (!SeleniumDriverInstance.pressEnter_2(Audit_Management_PageObjects.TypeSearch2()))
            {
                error = "Failed to press enter";
                return false;
            }
        
        
                
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.Text3(getData("Function Location"))))
        {
            error = "Failed to wait for Functional location drop down option : " + getData("Function Location");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.Text3(getData("Function Location"))))
        {
            error = "Failed to click  Functional location drop down option : " + getData("Function Location");
            return false;
        }

        narrator.stepPassedWithScreenShot("Functional location :" + getData("Function Location"));

        // Impact type
//        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.ImpactTypeDropDown()))
//        {
//            error = "Failed to wait for Impact type drop down.";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.ImpactTypeDropDown()))
//        {
//            error = "Failed to click Impact type drop down.";
//            return false;
//        }
//        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.ImpactTypeDropDownOption("S & SD")))
//        {
//            error = "Failed to wait for  Impact type drop down option : " + getData("Function Location");
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.ImpactTypeDropDownOption("S & SD")))
//        {
//            error = "Failed to click  Impact type drop down option : " + getData("Business Unit");
//            return false;
//        }
//
//        narrator.stepPassedWithScreenShot("Function Location :" + getData("Function Location"));
        //Audit title
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.AuditTitle()))
        {
            error = "Failed to wait for audit title textarea.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Audit_Management_PageObjects.AuditTitle(), getData("Audit title")))
        {
            error = "Failed to enter '" + getData("Audit title") + "' into Audit Title textarea.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Audit Title : '" + getData("Audit title") + "'.");

        //Audit scope
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.AuditScope()))
        {
            error = "Failed to wait for scope textarea.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Audit_Management_PageObjects.AuditScope(), getData("Audit scope")))
        {
            error = "Failed to enter '" + getData("Audit title") + "' into Audit scope textarea.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Audit scope : '" + getData("Audit scope") + "'.");

        if (getData("Link Project").equalsIgnoreCase("true"))
        {
            //link to project
            if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.LinkProjectCheckBox()))
            {
                error = "Failed to wait for link to project check box.";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.LinkProjectCheckBox()))
            {
                error = "Failed to click link to project check box.";
                return false;
            }
            narrator.stepPassedWithScreenShot("link to project check box.");

            //Project 
            if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.ProjectDropDown()))
            {
                error = "Failed to wait for project drop down.";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.ProjectDropDown()))
            {
                error = "Failed to click project drop down.";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.TypeSearch2()))
            {
                error = "Failed to wait for Business Unit text box.";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(Audit_Management_PageObjects.TypeSearch2(), getData("Project")))
            {
                error = "Failed to enter Project option :" + getData("Project");
                return false;
            }
            if (!SeleniumDriverInstance.pressEnter_2(Audit_Management_PageObjects.TypeSearch2()))
            {
                error = "Failed to press enter";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.Text3(getData("Project"))))
            {
                error = "Failed to wait for  project drop down option : " + getData("Project");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.Text3(getData("Project"))))
            {
                error = "Failed to click  project drop down option : " + getData("Project");
                return false;
            }

            narrator.stepPassedWithScreenShot("Project  :" + getData("Project"));

        }//end of if statement 

        pause(4000);

        //Audit type
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.AuditTypeDropDown()))
        {
            error = "Failed to wait for audit type drop down.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.AuditTypeDropDown()))
        {
            error = "Failed to click audit type drop down.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.AuditType(getData("Audit Type"))))
        {
            error = "Failed to wait for  audit type drop down option : " + getData("Audit Type");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.AuditType(getData("Audit Type"))))
        {
            error = "Failed to click  audit type drop down option : " + getData("Audit Type");
            return false;
        }

        narrator.stepPassedWithScreenShot("Audit Type  :" + getData("Audit Type"));

//        if (getData("Audit Type").equalsIgnoreCase("External"))
//        {
//            pause(3000);
//
//            //Audi template type
//            if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.AuditemplateDropDown()))
//            {
//                error = "Failed to wait for audit template drop down.";
//                return false;
//            }
//
//            if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.AuditemplateDropDown()))
//            {
//                error = "Failed to click audit template drop down.";
//                return false;
//            }
//
//            if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.AudiTemplateType(getData("Audi template type"))))
//            {
//                error = "Failed to wait for audit template type drop down :" + getData("Audi template type");
//                return false;
//            }
//
//            if (!SeleniumDriverInstance.doubleClickElementbyXpath(Audit_Management_PageObjects.AudiTemplateType(getData("Audi template type"))))
//            {
//                error = "Failed to click  audit template type drop down :" + getData("Audi template type");
//                return false;
//            }
//
//            narrator.stepPassedWithScreenShot("Audi template type  :" + getData("Audi template type"));
//
//            pause(3000);
//            if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.AudiTemplateOption(getData("Audi template"))))
//            {
//                error = "Failed to wait for audit template drop down option :" + getData("Audi template");
//                return false;
//            }
//
//            if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.AudiTemplateOption(getData("Audi template"))))
//            {
//                error = "Failed to click  audit template  drop down option :" + getData("Audi template");
//                return false;
//            }
//
//            narrator.stepPassedWithScreenShot("Audi template  :" + getData("Audi template"));
//
//        }
        if (getData("Audit Type").equalsIgnoreCase("Supplier"))
        {
            pause(2000);

            //Related stakeholder
            if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.RelatedStakeholderDropDown()))
            {
                error = "Failed to wait for Related Stakeholder drop down";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.RelatedStakeholderDropDown()))
            {
                if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.RelatedStakeholderDropDown()))
                {
                    error = "Failed to click  Related Stakeholder drop down.";
                    return false;
                }
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.TypeSearch()))
            {
                error = "Failed to wait for Related Stakeholder text box.";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(Audit_Management_PageObjects.TypeSearch(), getData("Related Stakeholder")))
            {
                error = "Failed to enter Related Stakeholder option :" + getData("Related Stakeholder");
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter_2(Audit_Management_PageObjects.TypeSearch()))
            {
                error = "Failed to press enter";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.businessUnitOption1(getData("Related Stakeholder"))))
            {
                error = "Failed to wait for Related Stakeholder drop down option : " + getData("Related Stakeholder");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.businessUnitOption1(getData("Related Stakeholder"))))
            {
                error = "Failed to click  Related Stakeholder drop down option : " + getData("Related Stakeholder");
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.Text2(getData("Related Stakeholder"))))
            {
                error = "Failed to wait for Related Stakeholder drop down option : " + getData("Related Stakeholder");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.Text2(getData("Related Stakeholder"))))
            {
                error = "Failed to click  Related Stakeholder drop down option : " + getData("Related Stakeholder");
                return false;
            }

            narrator.stepPassedWithScreenShot("Related Stakeholder  :" + getData("Related Stakeholder"));
            //narrator.stepPassedWithScreenShot("Related Stakeholder  : aa entity");

            pause(2000);

            //Audi template type
            if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.AuditemplateDropDown()))
            {
                error = "Failed to wait for audit template drop down.";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.AuditemplateDropDown()))
            {
                error = "Failed to click audit template drop down.";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.TypeSearch2()))
            {
                error = "Failed to wait for Type of event search text box";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(Audit_Management_PageObjects.TypeSearch2(), "She"))
            {
                error = "Failed to wait for Type of event search text box";
                return false;
            }

            pause(3000);
            if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.AudiTemplateTypeDropDown("SHE")))
            {
                error = "Failed to wait to expand audit template";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.AudiTemplateTypeDropDown("SHE")))
            {
                error = "Failed to expand audit template";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.AudiTemplateType(getData("Audi template type"))))
            {
                error = "Failed to wait for audit template type drop down :" + getData("Audi template type");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.AudiTemplateType(getData("Audi template type"))))
            {
                error = "Failed to click  audit template type drop down :" + getData("Audi template type");
                return false;
            }

            narrator.stepPassedWithScreenShot("Audi template type  :" + getData("Audi template type"));

            // pause(2000);
//            if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.AudiTemplateOption(getData("Audi template"))))
//            {
//                error = "Failed to wait for audit template drop down option :" + getData("Audi template");
//                return false;
//            }
//
//            if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.AudiTemplateOption(getData("Audi template"))))
//            {
//                error = "Failed to click  audit template  drop down option :" + getData("Audi template");
//                return false;
//            }
//
//            narrator.stepPassedWithScreenShot("Audi template  :" + getData("Audi template"));
        }

        pause(2000);

        //Process/activity
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.ProcessActivity()))
        {
            error = "Failed to wait for Process/Activity";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.ProcessActivity()))
        {
            error = "Failed to click  Process/Activity.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for Process/Activity text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Audit_Management_PageObjects.TypeSearch(), "a"))
        {
            error = "Failed to enter Process/Activity option :" + "a";
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(Audit_Management_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }

        pause(2000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.ProcessActivitySelectAll()))
        {
            error = "Failed to wait for Process/Activity select all";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.ProcessActivitySelectAll()))
        {
            error = "Failed to click Process/Activity select all";
            return false;
        }
        // narrator.stepPassedWithScreenShot("Process/Activity  :" + getData("Audit Type"));

        pause(2000);
        //Audit manager
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.AuditManagerDropDown()))
        {
            error = "Failed to wait for Audit manager drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.AuditManagerDropDown()))
        {
            error = "Failed to click  Audit manager drop down.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for Audit manager text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Audit_Management_PageObjects.TypeSearch(), getData("Audit manager")))
        {
            error = "Failed to wait for Audit manager option :" + getData("Audit manager");
            return false;
        }
        if (!SeleniumDriverInstance.pressEnter_2(Audit_Management_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.Text2(getData("Audit manager"))))
        {
            error = "Failed to wait for Audit manager  drop down option : " + getData("Audit manager");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.Text2(getData("Audit manager"))))
        {
            error = "Failed to click Audit manager  drop down option : " + getData("Audit manager");
            return false;
        }

        narrator.stepPassedWithScreenShot("Related Stakeholder  :" + getData("Related Stakeholder"));

        //Auditee
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.AuditeeDropDown()))
        {
            error = "Failed to wait for Audit manager drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.AuditeeDropDown()))
        {
            error = "Failed to click  Audit manager drop down.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for Audit manager text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Audit_Management_PageObjects.TypeSearch(), getData("Auditee")))
        {
            error = "Failed to wait for Audit manager option";
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter_2(Audit_Management_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.AudiTemplateType(getData("Auditee"))))
        {
            error = "Failed to wait for Auditee drop down option : " + getData("Auditee");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.AudiTemplateType(getData("Auditee"))))
        {
            error = "Failed to click Auditee drop down option : " + getData("Auditee");
            return false;
        }

        narrator.stepPassedWithScreenShot("Related Stakeholder  :" + getData("Related Stakeholder"));

        //Person conducting the audit
//        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.PersonConductingTheAuditDropDown()))
//        {
//            error = "Failed to wait for Person conducting the audit drop down";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.PersonConductingTheAuditDropDown()))
//        {
//            error = "Failed to click  Person conducting the audit drop down.";
//            return false;
//        }
//        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.TypeSearch_2()))
//        {
//            error = "Failed to wait for Person conducting the audittext box.";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.enterTextByXpath(Audit_Management_PageObjects.TypeSearch_2(), getData("Auditee")))
//        {
//            error = "Failed to enter Person conducting the audit";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.pressEnter())
//        {
//            error = "Failed to press enter";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.AudiTemplateType(getData("Auditee"))))
//        {
//            error = "Failed to wait for Person conducting the audit drop down option : " + getData("Auditee");
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.AudiTemplateType(getData("Auditee"))))
//        {
//            error = "Failed to click Person conducting the audit drop down option : " + getData("Auditee");
//            return false;
//        }
        //Audit start date     
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.AudiStartDate()))
        {
            error = "Failed to wait for  audit start date textarea.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Audit_Management_PageObjects.AudiStartDate(), startDate))
        {
            error = "Failed to enter '" + startDate + "' into start date textarea.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Audit start date : '" + endDate + "'.");

        //Audit end Date 
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.AudiEndDate()))
        {
            error = "Failed to wait for start date textarea.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Audit_Management_PageObjects.AudiEndDate(), endDate))
        {
            error = "Failed to enter '" + endDate + "' into due date textarea.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Audit end date : '" + endDate + "'.");

        //Introduction
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.IntroductionInput()))
        {
            error = "Failed to wait for Introduction textarea.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Audit_Management_PageObjects.IntroductionInput(), endDate))
        {
            error = "Failed to enter Introduction into due date textarea.";
            return false;
        }

        //Audit objective
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.AuditObjective()))
        {
            error = "Failed to wait for Audit objective textarea.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Audit_Management_PageObjects.AuditObjective(), endDate))
        {
            error = "Failed to enter Audit objective into due date textarea.";
            return false;
        }

        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.AuditButtonSave()))
        {
            error = "Failed to wait for Save button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.AuditButtonSave()))
        {
            error = "Failed to click Save button.";
            return false;
        }

        pause(2000);
        //Check if the record has been Saved
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.recordSaved_popup()))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        String saved = SeleniumDriverInstance.retrieveTextByXpath(Audit_Management_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");

            //Getting the action No
            String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Audit_Management_PageObjects.getActionRecord());
            String[] record = acionRecord.split(" ");
            Audit_Management_PageObjects.setRecord_Number(record[2]);
            String record_ = Audit_Management_PageObjects.getRecord_Number();

            narrator.stepPassed("Record number :" + acionRecord);

        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Audit_Management_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        if (getData("Submit Audit").equalsIgnoreCase("True"))
        {

            if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.ScheduleApprovedCheckBox()))
            {
                error = "Failed to wait for Schedule Approved CheckBox";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.ScheduleApprovedCheckBox()))
            {
                error = "Failed to click Schedule Approved CheckBox";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.SubmitAuditPlanning()))
            {
                error = "Failed to wait for Submit audit planning button";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.SubmitAuditPlanning()))
            {
                error = "Failed to click Submit audit planning button";
                return false;
            }
            String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Audit_Management_PageObjects.getActionRecord());
            String[] record = acionRecord.split(" ");
            Audit_Management_PageObjects.setRecord_Number(record[2]);
            String record_ = Audit_Management_PageObjects.getRecord_Number();
            narrator.stepPassed("Record number :" + acionRecord);

            narrator.stepPassedWithScreenShot("Submit audit planning button clicked");

            pause(6000);

            narrator.stepPassedWithScreenShot("Successfully captured audit");
        }//end of if else 

        return true;
    }
}//emd of method capture  audit details 

