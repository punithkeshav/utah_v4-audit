/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Audit_Management;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Audit_Management_PageObjects.Audit_Management_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author LDisemelo
 */
@KeywordAnnotation(
        Keyword = "FR10 Verification Approval - Main Scenario",
        createNewBrowserInstance = false
)
public class FR10_Verification_Approval_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR10_Verification_Approval_MainScenario()

    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());

    }

    public TestResult executeTest()
    {

        if (!RequestVerificationAprroval())
        {
            return narrator.testFailed("Failed to capture audit details due to : " + error);
        }
        return narrator.finalizeTest("Successfully navigate to Audit Management");
    }

    public boolean RequestVerificationAprroval()
    {

        // Navigate to the Verification TAB
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.VerificationTab()))
        {
            error = "Failed to wait for Verification Tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.VerificationTab()))
        {
            error = "Failed to click on 'Verification Tab";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked on Verification Tab");

        //Add Audit verification
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.VerificationAdd()))
        {
            error = "Failed to wait for verification button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.VerificationAdd()))
        {
            error = "Failed to click Verfication button.";
            return false;
        }

        //process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.VerificationProcessFlow()))
        {
            error = "Failed to wait for process flow";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.VerificationProcessFlow()))
        {
            error = "Failed to click process flow.";
            return false;
        }

        // Auditor Type dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.AuditorTypeDropDowm()))
        {
            error = "Failed to wait for auditor dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.AuditorTypeDropDowm()))
        {
            error = "Failed to click auditor dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.TypeSearch_2()))
        {
            error = "Failed to wait for Auditor Type drop search bar";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Audit_Management_PageObjects.TypeSearch_2(), getData("Auditor type")))
        {
            error = "Failed to enter Auditor type: " + getData("Auditor type");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.Text3(getData("Auditor type"))))
        {
            error = "Failed to wait for Auditor type option : " + getData("Auditor type");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.Text3(getData("Auditor type"))))
        {
            error = "Failed to click Aduitor type text option : " + getData("Auditor type");
            return false;
        }

        narrator.stepPassedWithScreenShot("Auditor type :" + getData("Auditor type"));

        // Auditor DropDown
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.AuditorDropDowm()))
        {
            error = "Failed to wait for auditor dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.AuditorDropDowm()))
        {
            error = "Failed to click auditor dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for Auditor Type drop search bar";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Audit_Management_PageObjects.TypeSearch(), getData("Auditor")))
        {
            error = "Failed to enter the text option : " + getData("Auditor");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter())

        {
            error = "Failed to press enter";
            return false;
        }
        pause(5000);

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.Text2(getData("Auditor"))))
        {
            error = "Failed to wait for Auditor  option : " + getData("Audit");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.Text2(getData("Auditor"))))
        {
            error = "Failed to click Auditor  option : " + getData("Auditor");
            return false;
        }

        narrator.stepPassed("Auditor :" + getData("Auditor"));

        // Verify dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.VerifyDropDown()))
        {
            error = "Failed to wait for Verify dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.VerifyDropDown()))
        {
            error = "Failed to click Verify dropdown.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.TypeSearch_2()))
        {
            error = "Failed to wait for Verify drop search bar";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Audit_Management_PageObjects.TypeSearch_2(), getData("Verify")))
        {
            error = "Failed to enter the text option : " + getData("Verify");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter())

        {
            error = "Failed to press enter";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.Text(getData("Verify"))))
        {
            error = "Failed to wait for Verifyt option : " + getData("Verify");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.Text(getData("Verify"))))
        {
            error = "Failed to click Verify option : " + getData("Verify");
            return false;
        }

        narrator.stepPassed("Verify  :" + getData("Verify"));

        // Comments 
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.VerificationComments()))
        {
            error = "Failed to wait for comments text area.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpathUsingActions(Audit_Management_PageObjects.VerificationComments(), getData("Comments")))
        {
            error = "Failed to enter Comments  : " + getData("Comments");
            return false;
        }

        narrator.stepPassed("Successfully Added Comments :" + getData("Comments"));

        // Email Comments 
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.EmailCommentsTo()))
        {
            error = "Failed to wait for Email comments to text area.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.EmailCommentsTo()))
        {
            error = "Failed to click  Email comments to text area.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for Email Comments  drop search bar";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Audit_Management_PageObjects.TypeSearch(), getData("Email comments")))
        {
            error = "Failed to enter Email comments : " + getData("Email comments");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter())

        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.TypeSearchCheckBox(getData("Email comments"))))
        {
            error = "Failed to wait for Email comments : " + getData("Email comments");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.TypeSearchCheckBox(getData("Email comments"))))
        {
            error = "Failed to click Email to comments option." + getData("Email comments");
            return false;
        }

        narrator.stepPassed("Email comments :" + getData("Email comments"));
        
         pause(3000);
        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.VerifiySaveButton()))
        {
            error = "Failed to click save button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked starte audit");

        //Check if the record has been Saved
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }

        String saved = SeleniumDriverInstance.retrieveTextByXpath(Audit_Management_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");

            //Getting the action No
            String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Audit_Management_PageObjects.getActionRecord());
            String[] record = acionRecord.split(" ");
            Audit_Management_PageObjects.setRecord_Number(record[2]);
            String record_ = Audit_Management_PageObjects.getRecord_Number();
            narrator.stepPassed("Record number :" + acionRecord);

        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Audit_Management_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        narrator.stepPassedWithScreenShot("Audit Started");

        return true;

    }

}
