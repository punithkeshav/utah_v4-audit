/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Audit_Management;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.Audit_Management_PageObjects.Audit_Management_PageObjects;

/**
 *
 * @author SMABE
 */
@KeywordAnnotation(
        Keyword = "FR11 Delete Audit",
        createNewBrowserInstance = false
)
public class FR11_Delete_Audit_MainScenario extends BaseClass
{

    String error = "";

    public FR11_Delete_Audit_MainScenario()
    {

    }

    public TestResult executeTest() throws InterruptedException
    {
        if (!DeleteAudit())
        {
            return narrator.testFailed("Failed To Delete Audit :" + error);
        }

        return narrator.finalizeTest("Successfully Audit Deleted");
    }

    public boolean DeleteAudit() throws InterruptedException
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.RecordDelete()))
        {
            error = "Failed to wait for the record delete button";
            return false;
        }
        String acionRecord = Audit_Management_PageObjects.getRecord_Number();
        String[] array = acionRecord.split("#");

//        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.CloseCurrentModule()))
//        {
//            error = "Failed to close current module";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.switchToTabOrWindow())
//        {
//            error = "Failed to switch tab.";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.ButtonConfirm()))
//        {
//            error = "Failed to wait for the yes button";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.ButtonConfirm()))
//        {
//            error = "Failed to click the yes button";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.switchToTabOrWindow())
//        {
//            error = "Failed to switch tab.";
//            return false;
//        }
//        
//        pause(5000);
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.CloseCurrentModule2()))
//        {
//            error = "Failed to wait for the cross to close current module";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.CloseCurrentModule2()))
//        {
//            error = "Failed to close current module";
//            return false;
//        }
//        pause(3000);
//        narrator.stepPassedWithScreenShot("Successfully closed current module");
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.AuditManagementLabel()))
//        {
//            error = "Failed To Wait For Waste Monitoring Tab";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.AuditManagementLabel()))
//        {
//            error = "Failed Click Waste Monitoring Tab";
//            return false;
//        }
//        pause(3000);
//
//        narrator.stepPassedWithScreenShot("Waste Monitoring Tab Clicked Successfully");
//
//        //contains text box
//        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.ContainsTextBox()))
//        {
//            error = "Failed to wait for contains text box";
//            return false;
//        }
//
//        //contains
//        if (!SeleniumDriverInstance.enterTextByXpath(Audit_Management_PageObjects.ContainsTextBox(), array[1]))
//        {
//            error = "Failed to enter contains to search";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.SearchButton()))
//        {
//            error = "Failed Click Search Button";
//            return false;
//        }
//        pause(3000);
//        narrator.stepPassedWithScreenShot("Searched For Record :" + array[1]);
//
//        //Record
//        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.Record(array[1])))
//        {
//            error = "Failed to wait for record";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.Record(array[1])))
//        {
//            error = "Failed click record";
//            return false;
//        }
//
//        narrator.stepPassedWithScreenShot("Record Found and clicked record : " + array[1]);

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.DeleteButton()))
        {
            error = "Failed To Wait For Delete Button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.DeleteButton()))
        {
            error = "Failed click Delete Button";
            return false;
        }

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch tab.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.ButtonConfirm()))
        {
            error = "Failed to wait for the yes button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.ButtonConfirm()))
        {
            error = "Failed to click the yes button";
            return false;
        }

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch tab.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.ButtonOK()))
        {
            error = "Failed to wait for the ok button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.ButtonOK()))
        {
            error = "Failed to click the ok button";
            return false;
        }
        narrator.stepPassedWithScreenShot("Recored Deleted ");
        pause(5000);

        return true;
    }

}
