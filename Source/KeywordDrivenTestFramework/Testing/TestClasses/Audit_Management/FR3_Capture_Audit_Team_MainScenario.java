/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Audit_Management;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Audit_Management_PageObjects.Audit_Management_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SMABE
 */
@KeywordAnnotation(
        Keyword = "Capture Audit Team  Main Scenario",
        createNewBrowserInstance = false
)

public class FR3_Capture_Audit_Team_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR3_Capture_Audit_Team_MainScenario()
    {
    }

    public TestResult executeTest()
    {

        if (!AuditManagement())
        {
            return narrator.testFailed("Failed to capture audit details due to : - " + error);
        }
        return narrator.finalizeTest("Successfully Captured Audit Team");
    }

    public boolean AuditManagement()
    {

        pause(4000);
        //Audit Team
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.AuditTeamTab()))
        {
            error = "Failed to wait for Audit Team tab.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.AuditTeamTab()))
        {
            error = "Failed to click for Audit Team  tab.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully clicked Audit Team Tab");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.AuditTeamAdd()))
        {
            error = "Failed to wait for Audit Team Add.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.AuditTeamAdd()))
        {
            error = "Failed to click  Audit Team  Add.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully clicked Audit Team Add");

        pause(5000);
        //Full Name

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.FullNameDropDowm()))
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.AuditTeamAdd()))
            {

                error = "Failed to wait for full name drop down";
                return false;
            }
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.FullNameDropDowm()))
        {
            error = "Failed to click full name drop down";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully clicked full name drop down");

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for full name drop down text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Audit_Management_PageObjects.TypeSearch(), getData("Full Name")))
        {
            error = "Failed to enter  full name :" + getData("Full Name");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter())
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.Text2(getData("Full Name"))))
        {
            error = "Failed to click  Full Name : " + getData("Full Name");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully seletcted full name :" + getData("Full Name"));

        //Experience/Role
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.ExperienceRoleDropDowm()))
        {
            error = "Failed to wait for Experience/Role drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.ExperienceRoleDropDowm()))
        {
            error = "Failed to click Experience/Role drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.Text(getData("Experience"))))
        {
            error = "Failed to wait for Experience/Role drop down option";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.Text(getData("Experience"))))
        {
            error = "Failed to click Experience/Role drop down option";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully clicked Experience/Role");

        //Person conducting the audit
        if (SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.PersonConductingTheAuditDropDown()))
        {

            if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.PersonConductingTheAuditDropDown()))
            {
                error = "Failed to click  Person conducting the audit drop down.";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.TypeSearch()))
            {
                error = "Failed to wait for Person conducting the audittext box.";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(Audit_Management_PageObjects.TypeSearch(), getData("Auditee")))
            {
                error = "Failed to enter Person conducting the audit";
                return false;
            }

            if (!SeleniumDriverInstance.pressEnter())
            {
                error = "Failed to press enter";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.AudiTemplateType(getData("Auditee"))))
            {
                error = "Failed to wait for Person conducting the audit drop down option : " + getData("Auditee");
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.AudiTemplateType(getData("Auditee"))))
            {
                error = "Failed to click Person conducting the audit drop down option : " + getData("Auditee");
                return false;
            }

        }

        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.AuditButtonSave()))
        {
            error = "Failed to wait for Save button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.AuditButtonSave()))
        {
            error = "Failed to click Save button.";
            return false;
        }
//        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.Actions_savewait()))
//        {
//            error = "Failed to wait for Save end.";
//            return false;
//        }

        //Check if the record has been Saved
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }

        String saved = SeleniumDriverInstance.retrieveTextByXpath(Audit_Management_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");

            //Getting the action No
            String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Audit_Management_PageObjects.getActionRecord());
            String[] record = acionRecord.split(" ");
            Audit_Management_PageObjects.setRecord_Number(record[2]);
            String record_ = Audit_Management_PageObjects.getRecord_Number();

            narrator.stepPassed("Record number :" + acionRecord);
            narrator.stepPassedWithScreenShot("Successfully captured audit");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Audit_Management_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }

        }

        return true;
    }

}
