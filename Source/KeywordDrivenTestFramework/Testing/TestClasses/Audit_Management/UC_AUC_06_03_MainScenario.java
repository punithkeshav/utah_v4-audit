/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Audit_Management;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Audit_Management_PageObjects.Audit_Management_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SMABE
 */
@KeywordAnnotation(
        Keyword = "UC_AUC_06_03 Main Scenario",
        createNewBrowserInstance = false
)
public class UC_AUC_06_03_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public UC_AUC_06_03_MainScenario()
    {
    }

    public TestResult executeTest()
    {

        if (!Findings())
        {
            return narrator.testFailed("Findings Failed Due To :  " + error);
        }
        return narrator.finalizeTest("Successfully Viewed Audit Findings where the audit type was internal or supplier");
    }

    public boolean Findings()
    {


        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.FindingsTab()))
        {
            error = "Failed to wait for Findings Tab.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.FindingsTab()))
        {
            error = "Failed to click Findings Tab.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.FindingsRecord()))
        {
            error = "Failed to wait for Findings Record";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully Viewed Audit Findings Record");
        
//        if (getData("Optional").equalsIgnoreCase("True"))
//        {
//            if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.FindingsRecord()))
//            {
//                error = "Failed to click Findings Record";
//                return false;
//            }
//
//            if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.FindindsButtonSaveDropDown()))
//            {
//                error = "Failed to wait for Save button drop down.";
//                return false;
//            }
//            if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.FindindsButtonSaveDropDown()))
//            {
//                error = "Failed to click Save button drop down.";
//                return false;
//            }
//
//            narrator.stepPassedWithScreenShot("Successfully Viewed Audit Findings Record");
//
//            if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.FindindsButtonSaveAndClose()))
//            {
//                error = "Failed to wait for save and close.";
//                return false;
//            }
//            if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.FindindsButtonSaveAndClose()))
//            {
//                error = "Failed to click save and close.";
//                return false;
//            }
//            if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.Audit_Management_Finding_Add_Button()))
//            {
//                error = "Failed to wait for Add Button";
//                return false;
//            }
//        }

        narrator.stepPassedWithScreenShot("Navigated Findings Record");

        return true;
    }
}
