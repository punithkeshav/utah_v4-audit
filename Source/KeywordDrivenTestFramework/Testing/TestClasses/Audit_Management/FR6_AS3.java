/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Audit_Management;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Audit_Management_PageObjects.Audit_Management_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SMABE
 */
@KeywordAnnotation(
        Keyword = "FR6 Conduct an internal or supplier  - Alternate Scenario 3",
        createNewBrowserInstance = false
)

public class FR6_AS3 extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR6_AS3()
    {
    }

    public TestResult executeTest()
    {

        if (!Continue())
        {
            return narrator.testFailed("Failed To Continue Due To : - " + error);
        }

        return narrator.finalizeTest("Successfully Continued");
    }

    public boolean Continue()
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.CloseRecord_4()))
        {
            error = "Failed to wait for the cross";
            return false;

        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.CloseRecord_4()))
        {
            error = "Failed to click cross";
            return false;

        }

        pause(10000);

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.AuditTeamTab()))
        {
            error = "Failed to wait for 'Audit Team'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Audit checklist closed using the cross in the top right-hand corner");

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.ContinueButton()))
        {
            error = "Failed to wait for Continue Button.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.ContinueButton()))
        {
            error = "Failed to click for Continue Button";
            return false;
        }

        pause(10000);

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.SaveCurrentSection()))
        {
            error = "Failed to wait for Save current section";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.SupportTab()))
        {
            error = "Failed to wait for the Support Tab";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully Clicked Continued");

        return true;
    }
}
