/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Audit_Management;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Audit_Management_PageObjects.Audit_Management_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SMABE
 */
@KeywordAnnotation(
        Keyword = "FR4-View Related Risks Main Scenario",
        createNewBrowserInstance = false
)

public class FR4_View_Related_Event_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR4_View_Related_Event_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {

        if (!RisksAndEvents())
        {
            return narrator.testFailed("Risks and Events Failed Due To : - " + error);
        }
        return narrator.finalizeTest("Successfully Viewed Related Risks");
    }

    public boolean RisksAndEvents()
    {

        //Audit Team
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.RisksTab()))
        {
            error = "Failed to wait for Risks Tab.";
            return false;
        }

        String window1 = SeleniumDriverInstance.Driver.getWindowHandle();

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.RisksTab()))
        {
            error = "Failed to click for Risks and Incidents Tab.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully Clicked Events Tab");

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.RelatedEventsDropDown()))
        {
            error = "Failed to wait for Related Risks.";
            return false;
        }

//        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.RelatedEventsDropDown()))
//        {
//            error = "Failed to click for Related Risks";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.EventsRecord()))
//        {
//            error = "Failed to wait for Events Record.";
//            return false;
//        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.EventsRecord()))
        {
            error = "Failed to wait for Events Record.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.EventsRecord()))
        {
            error = "Failed to click for Events Record.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully Clicked Events Record");

        pause(20000);

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch to record tab";
            return false;
        }
        pause(5000);

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch to record tab";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully Switched To Events Record Click");

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.iframeXpath()))
        {
            error = "Failed to switch to frame.";
            return false;

        }

        if (!SeleniumDriverInstance.switchToFrameByXpath(Audit_Management_PageObjects.iframeXpath()))
        {
            error = "Failed to switch to frame";
            return false;
        }

 
        
        pause(7000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.CloseRecord_2()))
        {
            error = "Failed to wait for the cross to click cross";
            return false;

        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.CloseRecord_2()))
        {
            error = "Failed to click cross";
            return false;

        }

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch tab.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.ButtonConfirm()))
        {
            error = "Failed to wait for the yes button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.ButtonConfirm()))
        {
            error = "Failed to click the yes button";
            return false;
        }

        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch tab.";
            return false;
        }

        if (!SeleniumDriverInstance.switchToFrameByXpath(Audit_Management_PageObjects.iframeXpath()))
        {
            error = "Failed to switch to frame";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.EventManagementHeading()))
        {
            error = "Failed to wait for Event Management heading";
            return false;
        }

        narrator.stepPassedWithScreenShot("Event Management Navigated");

        pause(5000);

        narrator.stepPassedWithScreenShot("Successfully Viewed Events Record");

        return true;

    }
}
