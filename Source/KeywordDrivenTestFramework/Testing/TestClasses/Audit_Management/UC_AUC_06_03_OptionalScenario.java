/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Audit_Management;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Audit_Management_PageObjects.Audit_Management_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SMABE
 */
@KeywordAnnotation(
        Keyword = "UC_AUC_06_03 Optional Scenario",
        createNewBrowserInstance = false
)
public class UC_AUC_06_03_OptionalScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public UC_AUC_06_03_OptionalScenario()
    {
    }

    public TestResult executeTest()
    {

        if (!Findings())
        {
            return narrator.testFailed("Findings Failed Due To : " + error);
        }
        return narrator.finalizeTest("Successfully Viewed Audit Findings where the audit type was internal or supplier");
    }

    public boolean Findings()
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.FindingsTab()))
        {
            error = "Failed to wait for Findings Tab.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.FindingsTab()))
        {
            error = "Failed to click Findings Tab.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.FindingsRecord()))
        {
            error = "Failed to wait for Findings Record.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.FindingsRecord()))
        {
            error = "Failed to click Findings Record.";
            return false;
        }

        // Finding Description
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.FindingDescription()))
        {
            error = "Failed to wait for Finding Descriptiontext box.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully Clicked Findings Record");

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.CloseCurrentModule()))
        {
            error = "Failed to wait for the cross to close current module";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.CloseCurrentModule()))
        {
            error = "Failed to close current module";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully Clicked Cross In Right Corner");

//        if (!SeleniumDriverInstance.switchToTabOrWindow())
//        {
//            error = "Failed to switch tab.";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.ButtonConfirm()))
//        {
//            error = "Failed to wait for the yes button";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.ButtonConfirm()))
//        {
//            error = "Failed to click the yes button";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.switchToTabOrWindow())
//        {
//            error = "Failed to switch tab.";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.switchToFrameByXpath(Audit_Management_PageObjects.iframeXpath()))
//        {
//            error = "Failed to switch to frame";
//            return false;
//        }
        pause(5000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.FindingsTab()))
        {
            error = "Failed to wait for Findings Tab.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully Viewed Audit Findings where the audit type was internal or supplier");

        return true;
    }
}
