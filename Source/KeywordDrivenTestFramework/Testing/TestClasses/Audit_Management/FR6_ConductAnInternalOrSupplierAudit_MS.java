/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Audit_Management;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Audit_Management_PageObjects.Audit_Management_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SMABE
 */
@KeywordAnnotation(
        Keyword = "FR6 Conduct an internal or supplier  - Main scenario",
        createNewBrowserInstance = false
)

public class FR6_ConductAnInternalOrSupplierAudit_MS extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR6_ConductAnInternalOrSupplierAudit_MS()
    {
    }

    public TestResult executeTest()
    {

        if (!AuditDetails())
        {
            return narrator.testFailed("Failed To Conduct An Internal Or Supplier Audit Due To : - " + error);
        }

        return narrator.finalizeTest("Successfully Conducted An Internal Or Supplier Audit");
    }

    public boolean AuditDetails()
    {
//
//        if (!SeleniumDriverInstance.PageUp())
//        {
//            error = "Failed to scroll up";
//            return false;
//        }
//      

        if (!SeleniumDriverInstance.scrollToElement(Audit_Management_PageObjects.AuditDetailsTab()))
        {
            error = "Failed to scroll to element";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.AuditTeamTab()))
        {
            error = "Failed to wait for 'Audit Team'.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.AuditTeamTab()))
        {
            error = "Failed to click 'Audit Team'.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.PersonConductingTheAuditDropDown()))
        {
            error = "Failed to wait for Person conducting the audit drop down.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.PersonConductingTheAuditDropDown()))
        {
            error = "Failed to click Person conducting the audit drop down.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for Person conducting the audit text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Audit_Management_PageObjects.TypeSearch(), getData("Person conducting the audit")))
        {
            error = "Failed to enter Person conducting the audit option :" + getData("Person conducting the audit");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter())
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.Text2(getData("Person conducting the audit"))))
        {
            error = " Failed to wait for Person conducting the audit :" + getData("Person conducting the audit");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.Text2(getData("Person conducting the audit"))))
        {
            error = "Failed to click Person conducting the audit :" + getData("Person conducting the audit");
            return false;
        }
        
                narrator.stepPassedWithScreenShot("Person conducting the audit :" + getData("Person conducting the audit"));


        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.StartButton()))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.StartButton_2()))
            {
                error = "Failed to wait for the Start Button";
                return false;
            }
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.StartButton()))
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.StartButton_2()))
            {
                error = "Failed to click the Start Button";
                return false;
            }
        }

//        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.SupportTab()))
//        {
//            error = "Failed to wait for the Support Tab";
//            return false;
//        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.Answer_1_DropDown()))
        {
            error = "Failed to wait for the Answer 1 Drop Down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.Answer_1_DropDown()))
        {
            error = "Failed to click Answer 1 Drop Down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.Text2(getData("Answer 1"))))
        {
            error = "Failed to wait for question 1 drop down option : " + getData("Answer 1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.Text2(getData("Answer 1"))))
        {
            error = "Failed to click  question 1  drop down option : " + getData("Answer 1");
            return false;
        }

        narrator.stepPassedWithScreenShot("Question 1 :" + getData("Answer 1"));

//        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.Comment()))
//        {
//            error = "Failed to wait Comment text area";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.enterTextByXpath(Audit_Management_PageObjects.Comment(), getData("Comment 1")))
//        {
//            error = "Failed to enter Comment :" + getData("Comment 1");
//            return false;
//        }
//
//        narrator.stepPassedWithScreenShot("Comment 1 :" + getData("Comment 1"));

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.SaveCurrentSection()))
        {
            error = "Failed to wait for Save current section";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.SaveCurrentSection()))
        {
            error = "Failed to click Save current section";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully Clicked Save current section");

        pause(10000);

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.SaveCurrentSection()))
        {
            error = "Failed to wait for Save current section";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.Comment()))
        {
            error = "Failed to wait Comment text area";
            return false;
        }

        if (!SeleniumDriverInstance.PageUp())
        {
            error = "Failed to scroll up";
            return false;
        }

        if (!SeleniumDriverInstance.PageUp())
        {
            error = "Failed to scroll up";
            return false;
        }

        if (getData("Close Audit Checklist").equalsIgnoreCase("True"))
        {

            if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.CloseRecord_4()))
            {
                error = "Failed to wait for the cross";
                return false;

            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.CloseRecord_4()))
            {
                error = "Failed to click cross";
                return false;

            }

            pause(10000);

            if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.AuditTeamTab()))
            {
                error = "Failed to wait for 'Audit Team'.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Audit checklist closed using the cross in the top right-hand corner");

        }

        return true;
    }
}
