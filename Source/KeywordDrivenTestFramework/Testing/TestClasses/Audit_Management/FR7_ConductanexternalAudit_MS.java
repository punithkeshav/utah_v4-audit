/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Audit_Management;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Audit_Management_PageObjects.Audit_Management_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SMABE
 */
@KeywordAnnotation(
        Keyword = "FR7-Conduct an external Audit",
        createNewBrowserInstance = false
)

public class FR7_ConductanexternalAudit_MS extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR7_ConductanexternalAudit_MS()
    {
    }

    public TestResult executeTest()
    {

        if (!ConductAudit())
        {
            return narrator.testFailed("Conduct Audit Failed Due To :" + error);
        }

        return narrator.finalizeTest("Successfully Capture the Audit where the audit type was internal or supplier");
    }

    public boolean ConductAudit()
    {
        if (!SeleniumDriverInstance.scrollUp())
        {
            error = "Failed to scroll up.";
            return false;
        }

        if (!SeleniumDriverInstance.scrollUp())
        {
            error = "Failed to scroll up.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.AuditTeamTab()))
        {
            error = "Failed to wait for 'Audit Team'.";
        }
        //return false;

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.AuditTeamTab()))
        {
            error = "Failed to click 'Audit Team'.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.PersonConductingTheAuditDropDown()))
        {
            error = "Failed to wait for Person conducting the audit drop down.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.PersonConductingTheAuditDropDown()))
        {
            error = "Failed to click Person conducting the audit drop down.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for Person conducting the audit text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Audit_Management_PageObjects.TypeSearch(), getData("Person conducting the audit")))
        {
            error = "Failed to enter Person conducting the audit option :" + getData("Person conducting the audit");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter())
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.Text2(getData("Person conducting the audit"))))
        {
            error = " Failed to wait for Person conducting the audit :" + getData("Person conducting the audit");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.Text2(getData("Person conducting the audit"))))
        {
            error = "Failed to click Person conducting the audit :" + getData("Person conducting the audit");
            return false;
        }

     
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.FindingsTab()))
        {
            error = "Failed to wait for Add Audit Management Finding";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.FindingsTab()))
        {
            error = "Failed to click Add Audit Management Finding";
            return false;
        }

        narrator.stepPassedWithScreenShot("Add Audit Management Finding");

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.Audit_Management_Finding_Add_Button()))
        {
            error = "Failed to wait for Add Button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.Audit_Management_Finding_Add_Button()))
        {
            error = "Failed to click Add Button";
            return false;
        }

        narrator.stepPassedWithScreenShot(
                "Successfully Clicked Add Button");

        //Process Flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.FindingsProcessFlow()))
        {
            error = "Failed to wait for Add Button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.FindingsProcessFlow()))
        {
            error = "Failed to click Add Button";
            return false;
        }

        // Finding Description
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.FindingDescription()))
        {
            error = "Failed to wait for Finding Descriptiontext box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Audit_Management_PageObjects.FindingDescription(), getData("Finding description")))
        {
            error = "Failed to enter  Finding Description :" + getData("Finding description");
            return false;
        }

        //FindingOwnerDropDown
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.FindingOwnerDropDown()))
        {
            error = "Failed to wait for Finding Owner drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.FindingOwnerDropDown()))
        {
            error = "Failed to click Finding Owner drop down";
            return false;
        }

        narrator.stepPassedWithScreenShot(
                "Successfully clicked Finding Owner drop down");

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for full name drop down text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Audit_Management_PageObjects.TypeSearch(), getData("Finding owner")))
        {
            error = "Failed to enter  Finding owner  :" + getData("Finding owner");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter())
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.Text2(getData("Finding owner"))))
        {
            error = "Failed to click  Finding owner : " + getData("Finding owner");
            return false;
        }

        narrator.stepPassedWithScreenShot(
                "Successfully seletcted  Finding owner :" + getData("Finding owner"));

        //FindingClassificationDropDown
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.FindingClassificationDropDown()))
        {
            error = "Failed to wait for Finding Classification drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.FindingClassificationDropDown()))
        {
            error = "Failed to click Finding Classification drop down";
            return false;
        }

        narrator.stepPassedWithScreenShot(
                "Successfully clicked Finding Classification drop down");

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.TypeSearch_2()))
        {
            error = "Failed to wait for Finding classification drop down text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Audit_Management_PageObjects.TypeSearch_2(), getData("Finding classification")))
        {
            error = "Failed to enter  Finding classification :" + getData("Finding classification");
            return false;
        }

        if (!SeleniumDriverInstance.pressEnter())
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.Text2(getData("Finding classification"))))
        {
            error = "Failed to click  Finding classification : " + getData("Finding classification");
            return false;
        }

        narrator.stepPassedWithScreenShot(
                "Successfully seletcted Finding classification  :" + getData("Finding classification"));

        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.FindindsButtonSave()))
        {
            error = "Failed to wait for Save button.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.FindindsButtonSave()))
        {
            error = "Failed to click Save button.";
            return false;
        }

        //Check if the record has been Saved
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.recordSaved_popup_2()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }

        String saved = SeleniumDriverInstance.retrieveTextByXpath(Audit_Management_PageObjects.recordSaved_popup_2());

        if (saved.equals(
                "Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");

            //Getting the action No
            String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Audit_Management_PageObjects.getActionRecord());
            String[] record = acionRecord.split(" ");
            Audit_Management_PageObjects.setRecord_Number(record[2]);
            String record_ = Audit_Management_PageObjects.getRecord_Number();

            narrator.stepPassed("Record number :" + acionRecord);

        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Audit_Management_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.FindindsButtonSaveDropDown()))
        {
            error = "Failed to wait for Save button drop down.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.FindindsButtonSaveDropDown()))
        {
            error = "Failed to click Save button drop down.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.FindindsButtonSaveAndClose()))
        {
            error = "Failed to wait for save and close.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.FindindsButtonSaveAndClose()))
        {
            error = "Failed to click save and close.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.Audit_Management_Finding_Add_Button()))
        {
            error = "Failed to wait for Add Button";
            return false;
        }
        pause(5000);

        narrator.stepPassed("Audit Management Finding Added");

        return true;
    }//end of method

}
