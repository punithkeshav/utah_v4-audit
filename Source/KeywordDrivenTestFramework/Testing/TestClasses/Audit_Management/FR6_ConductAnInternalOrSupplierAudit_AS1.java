/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Audit_Management;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Audit_Management_PageObjects.Audit_Management_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 *
 * @author SMABE
 */
@KeywordAnnotation(
        Keyword = "FR6 Conduct an internal or supplier  - Alternate Scenario 1",
        createNewBrowserInstance = false
)

public class FR6_ConductAnInternalOrSupplierAudit_AS1 extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR6_ConductAnInternalOrSupplierAudit_AS1()
    {
    }

    public TestResult executeTest()
    {

        if (!NavigateTabs())
        {
            return narrator.testFailed("Failed To Navigate Tabs Due To : " + error);
        }

        return narrator.finalizeTest("Successfully  Navigate Tabs");
    }

    public boolean NavigateTabs()
    {

        pause(20000);

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.CloseRecord_4()))
        {
            error = "Failed to wait for the cross";
            return false;

        }

        List<WebElement> Header = SeleniumDriverInstance.Driver.findElements(By.xpath(Audit_Management_PageObjects.TabsTotal()));
        List<WebElement> Header_1 = SeleniumDriverInstance.Driver.findElements(By.xpath("//div[contains(@class,'form active transition visible')]//div[@class='tabpanel_move_content tabpanel_move_content_scroll']//li"));


        int val_1 = 1, val_2 = 0;
        val_1 = Header.size();
        val_2 = val_1 - 1;
        for (int i = 0; i < val_2; i++)
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.NextButton()))
            {
                error = "Failed to wait for the Next Button";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.NextButton()))
            {
                error = "Failed to click the  Next Button";
                return false;

            }

        }
        for (int i = 0; i < val_2; i++)
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.PreviousButton()))
            {
                error = "Failed to wait for Previous Button";
                return false;

            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.PreviousButton()))
            {
                error = "Failed to click Previous Button";
                return false;

            }
        }

        narrator.stepPassedWithScreenShot("Successfully  Navigate Previous And Next Tabs");

        return true;

    }

}
