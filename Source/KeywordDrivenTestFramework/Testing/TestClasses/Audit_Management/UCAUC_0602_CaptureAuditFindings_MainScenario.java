/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Audit_Management;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Audit_Management_PageObjects.Audit_Management_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SMABE
 */
@KeywordAnnotation(
        Keyword = "UC_AUC_06_02_Capture_Audit_Findings_MainScenario",
        createNewBrowserInstance = false
)
public class UCAUC_0602_CaptureAuditFindings_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public UCAUC_0602_CaptureAuditFindings_MainScenario()
    {
    }

    public TestResult executeTest()
    {

        if (!AuditManagementFinding())
        {
            return narrator.testFailed("Failed to add aduit findings due to : - " + error);
        }
        return narrator.finalizeTest("Successfully Capture Audit Findings ");
    }

    public boolean AuditManagementFinding()
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.AddFinding()))
        {
            error = "Failed to wait for Add Audit Management Finding button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.AddFinding()))
        {
            error = "Failed to click Add Audit Management Finding button";
            return false;
        }

        narrator.stepPassedWithScreenShot("Add Audit Management Finding");

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.Audit_Management_Finding_Add_Button()))
        {
            error = "Failed to wait for Add Button";
            return false;
        }

        pause(3000);
        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.Audit_Management_Finding_Add_Button()))
        {
            error = "Failed to click Add Button";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully Clicked Add Button");

        //Process Flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.FindingsProcessFlow()))
        {
            error = "Failed to wait for Add Button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.FindingsProcessFlow()))
        {
            error = "Failed to click Add Button";
            return false;
        }

        // Finding Description
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.FindingDescription()))
        {
            error = "Failed to wait for Finding Descriptiontext box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Audit_Management_PageObjects.FindingDescription(), getData("Finding description")))
        {
            error = "Failed to enter  Finding Description :" + getData("Finding description");
            return false;
        }

        //FindingOwnerDropDown
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.FindingOwnerDropDown()))
        {
            error = "Failed to wait for Finding Owner drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.FindingOwnerDropDown()))
        {
            error = "Failed to click Finding Owner drop down";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully clicked Finding Owner drop down");

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for full name drop down text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Audit_Management_PageObjects.TypeSearch(), getData("Finding owner")))
        {
            error = "Failed to enter Finding owner :" + getData("Finding owner");
            return false;
        }
        if (!SeleniumDriverInstance.pressEnter())
        {
            error = "Failed to press enter";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.Text2(getData("Finding owner"))))
        {
            error = "Failed to click  Finding owner : " + getData("Finding owner");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully seletcted  Finding owner :" + getData("Finding owner"));

        //FindingClassificationDropDown
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.FindingClassificationDropDown()))
        {
            error = "Failed to wait for Finding Classification drop down";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.FindingClassificationDropDown()))
        {
            error = "Failed to click Finding Classification drop down";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully clicked Finding Classification drop down");

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.TypeSearch_2()))
        {
            error = "Failed to wait for Finding classification drop down text box.";
            return false;
        }

//        if (!SeleniumDriverInstance.enterTextByXpath(Audit_Management_PageObjects.TypeSearch_2(), getData("Finding classification")))
//        {
//            error = "Failed to enter  Finding classification :" + getData("Finding classification");
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.pressEnter())
//        {
//            error = "Failed to press enter";
//            return false;
//        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.Text2(getData("Finding classification"))))
        {
            error = "Failed to wait for Finding classification : " + getData("Finding classification");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.Text2(getData("Finding classification"))))
        {
            error = "Failed to click  Finding classification : " + getData("Finding classification");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully seletcted Finding classification  :" + getData("Finding classification"));

        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.FindindsButtonSave()))
        {
            error = "Failed to wait for Save button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.FindindsButtonSave()))
        {
            error = "Failed to click Save button.";
            return false;
        }

        //Check if the record has been Saved
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.recordSaved_popup_2()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }

        String saved = SeleniumDriverInstance.retrieveTextByXpath(Audit_Management_PageObjects.recordSaved_popup_2());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");

            //Getting the action No
            String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Audit_Management_PageObjects.getActionRecord());
            String[] record = acionRecord.split(" ");
            Audit_Management_PageObjects.setRecord_Number(record[2]);
            String record_ = Audit_Management_PageObjects.getRecord_Number();

            narrator.stepPassed("Record number :" + acionRecord);

        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Audit_Management_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.FindindsButtonSaveDropDown()))
        {
            error = "Failed to wait for Save button drop down.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.FindindsButtonSaveDropDown()))
        {
            error = "Failed to click Save button drop down.";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.FindindsButtonSaveAndClose()))
        {
            error = "Failed to wait for save and close.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.FindindsButtonSaveAndClose()))
        {
            error = "Failed to click save and close.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.Audit_Management_Finding_Add_Button()))
        {
            error = "Failed to wait for Add Button";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.Audit_Management_Finding_Close_Button()))
        {
            error = "Failed to click close cross at the right had corner.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Audit Management Finding Added");
        
        pause(3000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.CloseRecord_3()))
            {
                error = "Failed to wait for the cross";
                return false;

            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Audit_Management_PageObjects.CloseRecord_3()))
            {
                error = "Failed to click cross";
                return false;

            }

            pause(10000);

            if (!SeleniumDriverInstance.waitForElementByXpath(Audit_Management_PageObjects.AuditTeamTab()))
            {
                error = "Failed to wait for 'Audit Team'.";
                return false;
            }
            //narrator.stepPassedWithScreenShot("Audit checklist closed using the cross in the top right-hand corner");
        
        
        

        return true;
    }
}
