/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Audit_Management;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Audit_Management_PageObjects.Audit_Management_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;
/**
 *
 * @author SMABE
 */

@KeywordAnnotation(
        Keyword = "UC_AUC_06_02_OptionalScenario",
        createNewBrowserInstance = false
)
public class UC_AUC_06_02_OptionalScenario extends BaseClass
{
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public UC_AUC_06_02_OptionalScenario()
    {
    }

    public TestResult executeTest()
    {

   if (!AuditManagementFinding())
        {
            return narrator.testFailed("Failed to add aduit findings due to : - " + error);
        }
        return narrator.finalizeTest("Successfully Capture Audit Findings ");
    }
    
    public boolean  AuditManagementFinding ()
    {
        return true;
    }
}
