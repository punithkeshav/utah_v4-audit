/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Other_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SikuliDriverInstance;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import KeywordDrivenTestFramework.Testing.PageObjects.Audit_PageObjects.GmailPageObject;
import org.openqa.selenium.By;

/**
 *
 * @author fnell
 */

@KeywordAnnotation
(
    Keyword = "Sign In To Gmail Account",
    createNewBrowserInstance = true
)
public class GmailSignInTest extends BaseClass
{

    String error = "";

    public GmailSignInTest()
    {

    }

    public TestResult executeTest(){
        if (!NavigateToGmailSignInPage()){
            return narrator.testFailed("Failed due - " + error);
        }
        if (!GmailRegistration()){
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Successfully Registered an Gmail Account :" + getData("Email Address"));
    }

    public boolean NavigateToGmailSignInPage(){
        if (!SeleniumDriverInstance.navigateTo(GmailPageObject.GmailRegistration())){
            error = "Failed to navigate to gmail.";
            return false;
        }
        return true;
    }

    public boolean GmailRegistration(){
        //First Name
        if(!SeleniumDriverInstance.waitForElementByXpath(GmailPageObject.first_name())){
            error = "Failed to wait for 'First Name' input.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(GmailPageObject.first_name(), getData("First Name"))){
            error = "Failed to enter '" + getData("First Name") + "' into 'First Name' input.";
            return false;
        }
        narrator.stepPassedWithScreenShot("First Name: '" + getData("First Name") + "'.");
        
        //Last Name
        if(!SeleniumDriverInstance.waitForElementByXpath(GmailPageObject.last_Name())){
            error = "Failed to wait for 'Last Name' input.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(GmailPageObject.last_Name(), getData("Last Name"))){
            error = "Failed to enter '" + getData("Last Name") + "' into 'Last Name' input.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Last Name: '" + getData("Last Name") + "'.");
        pause(2000);
        
        //Email Address
        SeleniumDriverInstance.Driver.findElement(By.xpath(GmailPageObject.emailAddress())).clear();
                
        if(!SeleniumDriverInstance.waitForElementByXpath(GmailPageObject.emailAddress())){
            error = "Failed to wait for 'Email Address' input.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(GmailPageObject.emailAddress(), getData("Email Address"))){
            error = "Failed to enter '" + getData("Email Address") + "' into 'Email Address' input.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Email Address: '" + getData("Email Address") + "'.");
       
        //Password
        if(!SeleniumDriverInstance.waitForElementByXpath(GmailPageObject.password())){
            error = "Failed to wait for 'Password' input.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(GmailPageObject.password(), getData("Password"))){
            error = "Failed to enter '" + getData("Password") + "' into 'Password' input.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Password: '" + getData("Password") + "'.");
        
        //Confirm Password
        if(!SeleniumDriverInstance.waitForElementByXpath(GmailPageObject.confirm_Password())){
            error = "Failed to wait for 'Confirm Password' input.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(GmailPageObject.confirm_Password(), getData("Password"))){
            error = "Failed to enter '" + getData("Password") + "' into 'Confirm Password' input.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Confirm Password: '" + getData("Password") + "'.");
        
        //Next
        if(!SeleniumDriverInstance.waitForElementByXpath(GmailPageObject.next_Button())){
            error = "Failed to wait for 'Next' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(GmailPageObject.next_Button())){
            error = "Failed to click on 'Next' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Next button..");
        
        //Phone Number
        if(!SeleniumDriverInstance.waitForElementByXpath(GmailPageObject.PhoneNumber())){
            error = "Failed to wait for 'Phone Number' button.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(GmailPageObject.PhoneNumber(), getData("Phone Number"))){
            error = "Failed to click on 'Phone Number' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Phone Number input..");
        
        //Next
        if(!SeleniumDriverInstance.waitForElementByXpath(GmailPageObject.next_Button())){
            error = "Failed to wait for 'Next' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(GmailPageObject.next_Button())){
            error = "Failed to click on 'Next' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Next button..");
        pause(2000);
        
        if(!SeleniumDriverInstance.waitForElementByXpath(GmailPageObject.code())){
            error = "Failed to wait for 'Code' button.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(GmailPageObject.code(), getData("Code"))){
            error = "Failed to click on 'Code' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Code input..");
        
        if(!SeleniumDriverInstance.waitForElementByXpath(GmailPageObject.verify())){
            error = "Failed to wait for 'verify' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(GmailPageObject.verify())){
            error = "Failed to click on 'verify' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked verify input..");
        
        //Day
        if(!SeleniumDriverInstance.waitForElementByXpath(GmailPageObject.day())){
            error = "Failed to wait for 'Da' input.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(GmailPageObject.day(), getData("Day"))){
            error = "Failed to click on 'Day' input.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Day input..");
        
        //Month
        if(!SeleniumDriverInstance.waitForElementByXpath(GmailPageObject.Month())){
            error = "Failed to wait for 'Month' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(GmailPageObject.Month())){
            error = "Failed to click on 'Month' button.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(GmailPageObject.month_Select(getData("Month")))){
            error = "Failed to wait for 'Month' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(GmailPageObject.month_Select(getData("Month")))){
            error = "Failed to click on 'Month' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Month input..");
        
        //Year
        if(!SeleniumDriverInstance.waitForElementByXpath(GmailPageObject.year())){
            error = "Failed to wait for 'Year' input.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(GmailPageObject.year(), getData("Year"))){
            error = "Failed to click on 'Year' input.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Year input..");
        
        //Gender
        if(!SeleniumDriverInstance.waitForElementByXpath(GmailPageObject.Gender())){
            error = "Failed to wait for 'Gender' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(GmailPageObject.Gender())){
            error = "Failed to click on 'Gender' button.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(GmailPageObject.Gender_Select(getData("Gender")))){
            error = "Failed to wait for 'Gender' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(GmailPageObject.Gender_Select(getData("Gender")))){
            error = "Failed to click on 'Gender' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Gender input..");
        
        //Next
        if(!SeleniumDriverInstance.waitForElementByXpath(GmailPageObject.next_Button())){
            error = "Failed to wait for 'Next' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(GmailPageObject.next_Button())){
            error = "Failed to click on 'Next' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Next button..");
        pause(2000);

        //Yes im in
        if(!SeleniumDriverInstance.waitForElementByXpath(GmailPageObject.Yes_Button())){
            error = "Failed to wait for 'Yes, I’m in' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(GmailPageObject.Yes_Button())){
            error = "Failed to click on 'Yes, I’m in' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Yes, I’m in button..");       
        
        //I agree
        if(!SeleniumDriverInstance.scrollDown()){
            error = "Failed to scroll down.";
        }
        if(!SeleniumDriverInstance.scrollToElement(GmailPageObject.moreOptions_Button())){
            error = "Failed to wait for 'Yes, I’m in' button.";
            return false;
        }
        pause(2000);
        if(!SeleniumDriverInstance.scrollDown()){
            error = "Failed to scroll down.";
        }
        if(!SeleniumDriverInstance.scrollToElement(GmailPageObject.moreOptions_Button())){
            error = "Failed to wait for 'Yes, I’m in' button.";
            return false;
        }
        pause(2000);
        
        if(!SeleniumDriverInstance.waitForElementByXpath(GmailPageObject.Agree_Button())){
            error = "Failed to click on 'I Agree' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(GmailPageObject.Agree_Button())){
            error = "Failed to click on 'I Agree' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked I Agree button..");     
        pause(2000);
        
        if(!SeleniumDriverInstance.waitForElementByXpath(GmailPageObject.home())){
            error = "Failed to click on 'home' tab.";
            return false;
        }
        
        return true;

    }

}
