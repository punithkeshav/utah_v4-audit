/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Waist_Management;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.WaistManagementPageObjects.WasteManagementPageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SMABE
 */

    @KeywordAnnotation(
        Keyword = "WasteManagement",
        createNewBrowserInstance = false
)
public class Waste_Management extends BaseClass
{
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public Waste_Management()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        
    }

    public TestResult executeTest()
    {
        if (!Audit_Management())
        {
            return narrator.testFailed("Failed to navigate to Audit Management due to :  " + error);
        }
     
        return narrator.finalizeTest("Successfully navigate to Audit Management");
    }
    
      public boolean Audit_Management()
    {

        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagementPageObjects.AuditManagementLabel()))
        {
            error = "Failed to wait for  Audit Management label";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully navigated to Audit Management ");

        if (!SeleniumDriverInstance.clickElementbyXpath(WasteManagementPageObjects.AuditManagementLabel()))
        {
            error = "Failed to click Audit Management label";
            return false;
        }

        //Add Button
        if (!SeleniumDriverInstance.waitForElementByXpath(WasteManagementPageObjects.Actions_add()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        return true;
    }
}
