/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Waist_Management;

import KeywordDrivenTestFramework.Testing.TestClasses.Audit_Management.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Audit_Management_PageObjects.MailSlurper_PageObjects;

import microsoft.exchange.webservices.data.property.complex.availability.Suggestion;

/**
 *
 * @author MJivan
 */
@KeywordAnnotation(
        Keyword = "Sign In To MailSlurper v5 - Actions",
        createNewBrowserInstance = true
)
public class MailSlurperSignIn extends BaseClass
{
    String parentWindow;
    String error = "";

    public MailSlurperSignIn()
    {

    }

    public TestResult executeTest()
    {
        if (!NavigateToMailSlurperSignInPage())
        {
            return narrator.testFailed("Failed to navigate to MailSlurper Sign In Page");
        }

        // This step will sign into the specified gmail account with the provided credentials
        if (!SignInToMailSlurper())
        {
            return narrator.testFailed("Failed to sign into the MailSlurper Home Page");
        }
        //This step will click the newly added record
        if (!clickEmailLink())
        {
            return narrator.testFailed("Failed to click record link");
        }

        return narrator.finalizeTest("Successfully Navigated through the Isometrix web page");
    }

    public boolean NavigateToMailSlurperSignInPage()
    {
//        if (!SeleniumDriverInstance.navigateTo(MailSlurper_PageObjects.mailSlurper())) {
//            error = "Failed to navigate to MailSlurper Home Page.";
//            return false;
//        }
        if (!SeleniumDriverInstance.navigateTo(getData("URL")))
        {
            error = "Failed to navigate to Isometrix Home Page.";
            return false;
        }

        return true;
    }

    public boolean SignInToMailSlurper()
    {
        if (!SeleniumDriverInstance.enterTextByXpath(MailSlurper_PageObjects.Username(), testData.getData("Username")))
        {
            error = "Failed to enter text into email text field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully Added the Username: " + testData.getData("Username") + " to the Username Text Field");

        if (!SeleniumDriverInstance.enterTextByXpath(MailSlurper_PageObjects.Password(), testData.getData("Password")))
        {
            error = "Failed to enter text into email text field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully Added the Password: " + testData.getData("Password") + " to the Password Text Field");

        if (!SeleniumDriverInstance.clickElementbyXpath(MailSlurper_PageObjects.LoginBtn()))
        {
            error = "Failed to click sign in button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully Clicked The Sign In Button");
        pause(5000);

        return true;
    }

    public boolean clickEmailLink()
    {
        parentWindow = SeleniumDriverInstance.Driver.getWindowHandle();

//        if (getData("Status").equalsIgnoreCase("Overdue"))
//        {

//            //Search button
//            if (!SeleniumDriverInstance.waitForElementByXpath(MailSlurper_PageObjects.searchBtn()))
//            {
//                error = "Failed to wait for Search button.";
//                return false;
//            }
//            if (!SeleniumDriverInstance.clickElementbyXpath(MailSlurper_PageObjects.searchBtn()))
//            {
//                error = "Failed to click on Search button.";
//                return false;
//            }
//            narrator.stepPassedWithScreenShot("Successfully Clicked Search button");
//            
//            SeleniumDriverInstance.switchToTabOrWindow();
//            
//            if (!SeleniumDriverInstance.enterTextByXpath(MailSlurper_PageObjects.recordNo(), testData.getData("Record number")))
//            {
//                error = "Failed to enter text into search text field.";
//                return false;
//            }
//            narrator.stepPassedWithScreenShot("Successfully entered the Record number: " + testData.getData("Record number") + " to the search Text Field");
//            
//            //Search button
//            if (!SeleniumDriverInstance.waitForElementByXpath(MailSlurper_PageObjects.search_Btn()))
//            {
//                error = "Failed to wait for Search button.";
//                return false;
//            }
//            if (!SeleniumDriverInstance.clickElementbyXpath(MailSlurper_PageObjects.search_Btn()))
//            {
//                error = "Failed to click on Search button.";
//                return false;
//            }
//            narrator.stepPassedWithScreenShot("Successfully Clicked Search button");
//
//        }
//        pause(2000);
//        SeleniumDriverInstance.Driver.switchTo().window(parentWindow);
//
//        //Click the newly added record
//        if (!SeleniumDriverInstance.waitForElementByXpath(MailSlurper_PageObjects.recordLink()))
//        {
//            error = "Failed to wait for the record link.";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(MailSlurper_PageObjects.recordLink()))
//        {
//            error = "Failed to click on the record link.";
//            return false;
//        }
//        //Link back to record
//        if (!SeleniumDriverInstance.waitForElementByXpath(MailSlurper_PageObjects.linkBackToRecord()))
//        {
//            error = "Failed to wait Link back to record.";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(MailSlurper_PageObjects.linkBackToRecord()))
//        {
//            error = "Failed to click on Link back to record.";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.enterTextByXpath(MailSlurper_PageObjects.Username(), testData.getData("UserName")))
//        {
//            error = "Failed to enter text into email text field.";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Successfully Added the Username: " + testData.getData("UserName") + " to the Username Text Field");
//
//        if (!SeleniumDriverInstance.enterTextByXpath(MailSlurper_PageObjects.Password(), testData.getData("PassWord")))
//        {
//            error = "Failed to enter text into email text field.";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Successfully Added the Password: " + testData.getData("PassWord") + " to the Password Text Field");
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(MailSlurper_PageObjects.LoginBtn()))
//        {
//            error = "Failed to click sign in button.";
//            return false;
//        }
//    
//        narrator.stepPassedWithScreenShot("Successfully Clicked The Record Link: " + SeleniumDriverInstance.retrieveTextByXpath(MailSlurper_PageObjects.recordLink()));

//        pause(1000);
      return true;

    }

       
}
