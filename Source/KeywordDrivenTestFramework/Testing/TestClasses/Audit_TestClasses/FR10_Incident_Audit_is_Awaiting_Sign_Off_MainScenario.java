/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Audit_TestClasses;


import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Audit_PageObjects.Audit_PageObjects;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Incident Audit is Awaiting Sign Off",
        createNewBrowserInstance = false
)

public class FR10_Incident_Audit_is_Awaiting_Sign_Off_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public FR10_Incident_Audit_is_Awaiting_Sign_Off_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!NavigateAuditFindings())
        {
            return narrator.testFailed("Failed due - " + error);
        }    
        
        return narrator.finalizeTest("Completed Audit is Awaiting Sign Off");
    }

    public boolean NavigateAuditFindings(){      
        //Navigate to Audits Actions
        pause(2000);
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.audit_findings())){
            error = "Failed to wait for 'Audits Findings' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.audit_findings())){
            error = "Failed to click on 'Audits Findings' tab.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Audits Findings' tab.");
        
        //Add button
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.audit_signoff())){
            error = "Failed to wait for 'Audit Sign Off' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.audit_signoff())){
            error = "Failed to click on 'Audit Sign Off' button.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully click 'Audit Sign Off' button.");
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.proto())){
            error = "Failed to wait for 'Audit Protocol' tab.";
            return false;
        }
        pause(3000);
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.proto())){
            error = "Failed to click on 'Audit Protocol' tab.";
            return false;
        }
        pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Audit Protocol' tab.");

        return true;
    }

}
