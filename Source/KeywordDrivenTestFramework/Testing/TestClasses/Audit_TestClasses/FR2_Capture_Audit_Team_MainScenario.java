/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Audit_TestClasses;


import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Audit_PageObjects.Audit_PageObjects;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Capture Audit Team Main",
        createNewBrowserInstance = false
)

public class FR2_Capture_Audit_Team_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public FR2_Capture_Audit_Team_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {
        if (!IsometrixNavigateSuggestionsAndInnovations())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        
        if(!enterDetails()){
            return narrator.testFailed("Failed due - " + error);
        }
        
        return narrator.finalizeTest("Completed navigate to Audit");
    }

    public boolean IsometrixNavigateSuggestionsAndInnovations(){

        //Navigate to Audit Team
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_TeamTab())){
            error = "Failed to wait for Audit Team tab.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audit_TeamTab())){
            error = "Failed to click Audit Team tab.";
            return false;
        }
        pause(2000);
        //Add button
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_Team_Add())){
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audit_Team_Add())){
            error = "Failed to click on 'Add' button.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        return true;
    }
    
    //Enter data
    public boolean enterDetails(){
//        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_Team_ProcessFlow())){
//            error = "Failed to wait for ProcessFlow.";
//            return false;
//        }
//        
//        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audit_Team_ProcessFlow())){
//            error = "Failed to click ProcessFlow.";
//            return false;
//        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_Team_Fullname_dropdown())){
            error = "Failed to wait for Full name dropdown.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audit_Team_Fullname_dropdown())){
            error = "Failed to click Full name dropdown.";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_Team_Fullname_Select(testData.getData("Full name")))){
            error = "Failed to wait for '"+testData.getData("Full name")+"' in Full name dropdown.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audit_Team_Fullname_Select(testData.getData("Full name")))){
            error = "Failed to click '"+testData.getData("Full name")+"' from Full name dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Full name entered.");
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_Team_Role())){
            error = "Failed to wait for Role input.";
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextByXpath(Audit_PageObjects.Audit_Team_Role(), testData.getData("Role"))){
            error = "Failed to enter '"+testData.getData("Role")+"' into Role input.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Role entered.");
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_Team_sdate())){
            error = "Failed to wait for Start date input.";
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextByXpath(Audit_PageObjects.Audit_Team_sdate(), testData.getData("Start date"))){
            error = "Failed to enter '"+testData.getData("Start date")+"' into Start date.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Start date entered.");
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_Team_edate())){
            error = "Failed to wait for End date input.";
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextByXpath(Audit_PageObjects.Audit_Team_edate(), testData.getData("End date"))){
            error = "Failed to enter '"+testData.getData("End date")+"' into End date.";
            return false;
        }
        narrator.stepPassedWithScreenShot("End date entered.");
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_Team_save())){
            error = "Failed to wait for Save button.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audit_Team_save())){
            error = "Failed to click Save button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Save clicked.");
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_TeamTab())){
            error = "Failed to wat for save finished.";
            return false;
        }    
        return true;
    }

}
