/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Audit_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Audit_PageObjects.Audit_PageObjects;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR7 View Related Incidents - Main Scenario",
        createNewBrowserInstance = false
)

public class FR7_ViewRelatedIncidents_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR7_ViewRelatedIncidents_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {
        if (!NavigateRelatedObligations())
        {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Completed navigate to View Related Obligations");
    }

    public boolean NavigateRelatedObligations()
    {
        //Navigate to Audits
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_ROI_tab()))
        {
            error = "Failed to wait for 'Risk, Obligation & Incidents.' tab";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audit_ROI_tab()))
        {
            error = "Failed to click on 'Risk, Obligation & Incidents' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Audits' tab.");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_RIL_dropdown()))
        {
            error = "Failed to wait for 'Related Incident Linked to Business' dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audit_RIL_dropdown()))
        {
            error = "Failed to click on 'Related Incident Linked to Business' dropdown.";
            return false;
        }
        SeleniumDriverInstance.pause(20000);
        
//        //Select record
//        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_RIL_select()))
//        {
//            error = "Failed to wait for 'Related Incident Linked to Business' record.";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audit_RIL_select()))
//        {
//            error = "Failed to click on 'Related Incident Linked to Business' record.";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Successfully click 'Related Incident Linked to Business' record.");
//
//        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Audit_PageObjects.saveWait2(), 400))
//        {
//            error = "Webside too long to load wait reached the time out";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_RIL_wait(), 5000))
//        {
//            error = "Failed to wait for 'Obligation Manager' tab.";
//            return false;
//        }
//        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Audit_PageObjects.Audit_RIL_wait(), 5000))
//        {
//            error = "Failed to wait for 'Obligations Manager' tab.";
//            return false;
//        }

        return true;
    }

}
