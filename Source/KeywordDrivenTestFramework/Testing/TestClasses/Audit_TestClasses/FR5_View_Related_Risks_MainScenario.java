/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Audit_TestClasses;


import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Audit_PageObjects.Audit_PageObjects;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "View Related Risks - Main Scenario",
        createNewBrowserInstance = false
)

public class FR5_View_Related_Risks_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public FR5_View_Related_Risks_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {
        if (!navigateToRiskObligationsIncidents())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Completed Capture Audit");
    }

    public boolean navigateToRiskObligationsIncidents(){
        //Navigate to Risk Obligation & Incidents
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.riskInciedentTab())){
            error = "Failed to wait for 'Risk Obligation & Incidents' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.riskInciedentTab())){
            error = "Failed to click on 'Risk Obligation & Incidents' tab.";
            return false;
        }
        
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Risk Obligation & Incidents' tab.");
        
        //Related Risk Panel
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.relatedRiskLinkedToBusiness())){
            error = "Failed to wait for 'Related Risk To Business' Panel.";
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.relatedRiskLinkedToBusiness())){
            error = "Failed to expand 'Related Risk To Business' Panel.";
        }
        
        SeleniumDriverInstance.pause(20000);
        return true;
        
    }

}
