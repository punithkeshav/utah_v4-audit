/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Audit_TestClasses;


import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Audit_PageObjects.Audit_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;

/**
 *
 * @author Ethiene
 */
@KeywordAnnotation(
        Keyword = "Optional scenario",
        createNewBrowserInstance = false
)
public class UC_AUR_01_02UpdateAuditOptionalScenario extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String path;

    public UC_AUR_01_02UpdateAuditOptionalScenario() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        String pathofImages = System.getProperty("user.dir") + "\\SikuliImages\\images";
        path = new File(pathofImages).getAbsolutePath();
    }

    public TestResult executeTest() {

        if (!supportingDocuments()) {
            return narrator.testFailed("Supporting Documents Failed to - " + error);
        }

        return narrator.finalizeTest("Successfully Registered A Risk");
    }

    public boolean supportingDocuments() {

        if (!SeleniumDriverInstance.scrollToElement(Audit_PageObjects.supportingDocumentTab())) {
            error = "Failed to display 'Supporting Documents' tab.";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.supportingDocumentTab())) {
            error = "Failed to click 'Supporting Documents' tab.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully click 'Supprting documents' tab.");
        
        //Link to a document button
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.link_Btn())){
            error = "Failed to wait for 'Link to a document' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.link_Btn())){
            error = "Failed to click on 'Link to a document' button.";
            return false;
        }
        SeleniumDriverInstance.switchToTabOrWindow();
        narrator.stepPassedWithScreenShot("Successfully click 'Link to a document' button.");
        //URL field
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.url_field())){
            error = "Failed to wait for 'URL' field.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Audit_PageObjects.url_field(), getData("URL"))){
            error = "Failed to click on '"+getData("URL")+"'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered '"+getData("URL")+"'.");
        //Title field
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.title_field())){
            error = "Failed to wait for 'Title' field.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Audit_PageObjects.title_field(), getData("Title"))){
            error = "Failed to click on '"+getData("Title")+"'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered '"+getData("Title")+"'.");
        //Add
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.BPM_add())){
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.BPM_add())){
            error = "Failed to click on 'Add' button.";
            return false;
        }
         narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");
         //switch to the iframe
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.iframeXpath()))
        {
            error = "Failed to wait for frame.";
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(Audit_PageObjects.iframeXpath()))
        {
            error = "Failed to switch to frame.";
        }

        narrator.stepPassedWithScreenShot("Completed uploading supporting documents");

      
        return true;
    }

}
