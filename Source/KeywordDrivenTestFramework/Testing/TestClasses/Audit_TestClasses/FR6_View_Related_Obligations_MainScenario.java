/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Audit_TestClasses;


import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Audit_PageObjects.Audit_PageObjects;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "View Releated Obligations",
        createNewBrowserInstance = false
)

public class FR6_View_Related_Obligations_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public FR6_View_Related_Obligations_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {
        if (!NavigateRelatedObligations())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        
        return narrator.finalizeTest("Completed navigate to View Related Obligations");
    }

    public boolean NavigateRelatedObligations(){
        //Navigate to Audits
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_ROI_tab())){
            error = "Failed to wait for 'Risk, Obligation & Incidents.' tab";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audit_ROI_tab())){
            error = "Failed to click on 'Risk, Obligation & Incidents' tab.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Audits' tab.");
        
        //Add button
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_ROI_dropdown())){
            error = "Failed to wait for 'Related Obligations' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audit_ROI_dropdown())){
            error = "Failed to click on 'Related Obligations' dropdown.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Related Obligations' dropdown.");

        //Select record
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_ROI_select(), 20000)){
            error = "Failed to wait for 'Related Obligations' record.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audit_ROI_select())){
            error = "Failed to click on 'Related Obligations' record.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Related Obligations' record.");
        
        if (SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.saveWait(), 2)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Audit_PageObjects.saveWait2(), 400)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_ROI_wait(), 500)){
            error = "Failed to wait for 'Obligation Manager' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementPresentByXpath(Audit_PageObjects.Audit_ROI_wait(), 500)){
            error = "Failed to wait for 'Obligations Manager' tab.";
            return false;
        }
        
        return true;
    }    

}
