/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Audit_TestClasses;


import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Audit_PageObjects.Audit_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Capture Audit Alt2",
        createNewBrowserInstance = false
)

public class FR1_Capture_Audit_AlternativeScenario2 extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public FR1_Capture_Audit_AlternativeScenario2()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {
        if (!IsometrixNavigateSuggestionsAndInnovations())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        
        if(!enterDetails()){
            return narrator.testFailed("Failed due - " + error);
        }
        
        return narrator.finalizeTest("Completed navigate to Audit");
    }

    public boolean IsometrixNavigateSuggestionsAndInnovations(){
        //Navigate to Environmental Health & Safety
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.navigate_EHS())){
            error = "Failed to wait for 'Environmental, Health & Safety' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.navigate_EHS())){
            error = "Failed to click on 'Environmental, Health & Safety' tab.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Environmental, Health & Safety' tab.");
        
        //Navigate to Audit & Inspections
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audits_Inspections())){
            error = "Failed to wait for 'Audit & Inspections.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audits_Inspections())){
            error = "Failed to click on 'Audit & Inspections' tab.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Audit & Inspections' tab.");
       
        //Navigate to Audits
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audits())){
            error = "Failed to wait for 'Audits.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audits())){
            error = "Failed to click on 'Audits' tab.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Audits' tab.");
        
        //Add button
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_Add())){
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audit_Add())){
            error = "Failed to click on 'Add' button.";
            return false;
        }
        SeleniumDriverInstance.pause(3000);
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        return true;
    }
    
    //Enter data
    public boolean enterDetails(){
        //Process flow
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.processFlow())){
            error = "Failed to wait for 'Process Flow' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.processFlow())){
            error = "Failed to click on 'Process Flow' tab.";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_BI_dropdown())){
            error = "Failed to wait for 'Business Unit' dropdown.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audit_BI_dropdown())){
            error = "Failed to click Business Unit dropdown.";
            return false;
        }
        
        //Stakeholder Individual Business Unit
        if(!testData.getData("Business Unit").equals("Global Company")){
            if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_BU_expand("Global Company"))){
                error = "Failed to wait to expand 'Global Company'.";
                return false;
            }
            if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audit_BU_expand("Global Company"))){
                error = "Failed to expand 'Global Company'.";
                return false;
            }
            if(!testData.getData("Business Unit").equals("South Africa")){
                if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_BU_expand("South Africa"))){
                    error = "Failed to wait to expand 'South Africa'.";
                    return false;
                }
                if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audit_BU_expand("South Africa"))){
                    error = "Failed to expand 'South Africa'.";
                    return false;
                }
                if(!testData.getData("Business Unit").equals("Victory Site")){
                    error = "Failed to find Business Unit";
                    return false;
                }else
                {
                    if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_BU_select(testData.getData("Business Unit")))){
                        error = "Failed to wait for '"+testData.getData("Business Unit")+"' option.";
                        return false;
                    }
                    if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audit_BU_select(testData.getData("Business Unit")))){
                        error = "Failed to click '"+testData.getData("Business Unit")+"' option.";
                        return false;
                    }
                }
            }else
            {
                if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_BU_select(testData.getData("Business Unit")))){
                    error = "Failed to wait for '"+testData.getData("Business Unit")+"' option.";
                    return false;
                }
                if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audit_BU_select(testData.getData("Business Unit")))){
                    error = "Failed to click '"+testData.getData("Business Unit")+"' option.";
                    return false;
                }
            }
        }else
        {
            if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_BU_select(testData.getData("Business Unit")))){
                error = "Failed to wait for '"+testData.getData("Business Unit")+"' option.";
                return false;
            }
            if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audit_BU_select(testData.getData("Business Unit")))){
                error = "Failed to click '"+testData.getData("Business Unit")+"' option.";
                return false;
            }
        }
        narrator.stepPassedWithScreenShot("Business unit entered.");
        
        //Impact type
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_ImpactType_dropdown())){
            error = "Failed to wait for 'Impact Type' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audit_ImpactType_dropdown())){
            error = "Failed to click Impact Type dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.EnvironmentalOption())){
            error = "Failed to wait for 'Environmental' option.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_ImpactType_selectall())){
            error = "Failed to wait for Impact Type select all button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audit_ImpactType_selectall())){
            error = "Failed to click Impact Type select all button.";
            return false;
        }
        pause(2000);
        narrator.stepPassedWithScreenShot("Impact type selected.");
        
        //Audit Title
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_title())){
            error = "Failed to wait for 'Audit Title' textarea.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Audit_PageObjects.Audit_title(), testData.getData("Audit Title"))){
            error = "Failed to enter text into 'Audit Title' textarea.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Audit Title entered.");
        
        //Audit scope
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_Scope())){
            error = "Failed to wait for 'Audit scope' textarea.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Audit_PageObjects.Audit_Scope(), testData.getData("Scope"))){
            error = "Failed to enter text into 'Audit scope' textarea.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Audit scope entered.");

        //Audit Type
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_Type_dropdown())){
            error = "Failed to wait for 'Audit Type' dropdown.";
            return false;
        }        
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audit_Type_dropdown())){
            error = "Failed to click 'Audit Type' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_Type_select(testData.getData("Audit Type")))){
            error = "Failed to wait for '"+testData.getData("Audit Type")+"' in Audit Type dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audit_Type_select(testData.getData("Audit Type")))){
            error = "Failed to select '"+testData.getData("Audit Type")+"' from Audit Type dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Audit Type selected.");
        
        //Audit Protocol
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_Protocol_dropdown())){
            error = "Failed to wait for 'Audit Protocol' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audit_Protocol_dropdown())){
            error = "Failed to click 'Audit Protocol' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_Protocol_Select(testData.getData("Audit Protocol")))){
            error = "Failed to wait for '"+testData.getData("Audit Protocol")+"' in Audit Protocol dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audit_Protocol_Select(testData.getData("Audit Protocol")))){
            error = "Failed to click '"+testData.getData("Audit Protocol")+"' from Audit Protocol dropdwon.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Audit Protocol selected.");
        
        //Process/Activity
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_PA_selectall())){
            error = "Failed to wait for 'Process/Activity' area.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audit_PA_selectall())){
            error = "Failed to click 'Process/Activity' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Process/Activity selected.");
        
        //Audit Manager
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_manager_dropdown())){
            error = "Failed to wait for 'Audit Manager' dropwdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audit_manager_dropdown())){
            error = "Failed click for 'Audit Manager' dropwdown.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_manager_select(testData.getData("Audit Manager")))){
            error = "Failed to wait for '"+testData.getData("Audit Manager")+"' in Audit Manager dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audit_manager_select(testData.getData("Audit Manager")))){
            error = "Failed to click '"+testData.getData("Audit Manager")+"' from Audit Manager dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Audit Manager selected.");
        
        //Auditee
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_Auditee_dropdown())){
            error = "Failed to wait for 'Auditee' dropwdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audit_Auditee_dropdown())){
            error = "Failed click for 'Auditee' dropwdown.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_Auditee_select(testData.getData("Auditee")))){
            error = "Failed to wait for '"+testData.getData("Auditee")+"' in Auditee dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audit_Auditee_select(testData.getData("Auditee")))){
            error = "Failed to click '"+testData.getData("Auditee")+"' from Auditee dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Auditee selected.");
        
        //Person
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_Person_dropdown())){
            error = "Failed to wait for 'Person' dropwdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audit_Person_dropdown())){
            error = "Failed click for 'Person' dropwdown.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_Person_select(testData.getData("Person")))){
            error = "Failed to wait for '"+testData.getData("Person")+"' in Person dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audit_Person_select(testData.getData("Person")))){
            error = "Failed to click '"+testData.getData("Person")+"' from Person dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Person selected.");
        
        //Audit Start Date
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_startdate())){
            error = "Failed to wait for 'Audit Start Date' textarea.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Audit_PageObjects.Audit_startdate(), testData.getData("Audit Start Date"))){
            error = "Failed to enter text into 'Audit Start Date' textarea.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Audit start date entered.");
        
        //Audit End Date
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_enddate())){
            error = "Failed to wait for 'Audit End Date' textarea.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Audit_PageObjects.Audit_enddate(), testData.getData("Audit End Date"))){
            error = "Failed to enter text into 'Audit End Date' textarea.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Audit end date entered.");
        
        //Introduction & Audit Objective
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_IntroTab())){
            error = "Failed to wait for 'Introduction & Audit Objective' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audit_IntroTab())){
            error = "Failed to click 'Introduction & Audit Objective' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_Intro_textarea())){
            error = "Failed to wait for 'Introduction' textarea.";
            return false;
        }        
        if(!SeleniumDriverInstance.enterTextByXpath(Audit_PageObjects.Audit_Intro_textarea(), testData.getData("Introduction"))){
            error = "Failed to enter '"+testData.getData("Introduction")+"' text into Introduction textarea.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Introduction entered.");
        
        //Audit Objective
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_Obj_textarea())){
            error = "Failed to wait for 'Audit Objective' textarea.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Audit_PageObjects.Audit_Obj_textarea(), testData.getData("Objective"))){
            error = "Failed to enter '"+testData.getData("Objective")+"' text into Introduction textarea.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Audit Objective entered.");
        narrator.stepPassedWithScreenShot("Details entered.");
        
        //Save to continue
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_SaveToContinue())){
            error = "Failed to wait for 'Save to continue' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audit_SaveToContinue())){
            error = "Failed to click 'Save to continue' button.";
            return false;
        }
        //Saving mask
        if (SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.saveWait(), 2)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Audit_PageObjects.saveWait2(), 400)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }
        narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_TeamTab())){
            error = "Failed to wait for Audit Team tab.";
            return false;
        }
        if(!SeleniumDriverInstance.scrollToElement(Audit_PageObjects.Audit_TeamTab())){
            error = "Failed to scroll to Audit Team tab.";
            return false;
        } 
        
        return true;
    }

}
