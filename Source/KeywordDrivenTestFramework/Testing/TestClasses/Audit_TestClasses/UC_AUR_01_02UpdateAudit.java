/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Audit_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Audit_PageObjects.Audit_PageObjects;
import KeywordDrivenTestFramework.Utilities.AppiumDriverUtility;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.sql.Driver;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "Update Audit Main",
        createNewBrowserInstance = false
)

public class UC_AUR_01_02UpdateAudit extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public UC_AUR_01_02UpdateAudit() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");

        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());

    }

    public TestResult executeTest() {
        if (!proposeDates()) {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Completed navigate to Audit and saved record :#" + getRecordId());
    }

    public boolean proposeDates() {
        String newDate = getData("New date");
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.proposeNewDatesCheckbox(), 2000)) {
            error = "Failed to wait for 'Propose new dates' checkbox.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.proposeNewDatesCheckbox())) {
            error = "Failed to click 'Propose new dates' checkbox.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Succsessfully checked Propose new date checkbox");
        if (newDate.equalsIgnoreCase("Yes")) {     
            
            if (!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.suggestedStartDate())) {
                error = "Failed to wait for 'Suggested start date'.";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(Audit_PageObjects.suggestedStartDate(), startDate)) {
                error = "Failed to enter  'Suggested start date'.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Succsessfully entered  '"+startDate+"' into start date field");
            
            if (!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.suggestedEndDate())) {
                error = "Failed to wait for 'Suggested end date'.";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(Audit_PageObjects.suggestedEndDate(), endDate)) {
                error = "Failed to enter 'Suggested end date'.";
                return false;
            }  
            narrator.stepPassedWithScreenShot("Succsessfully entered  '"+endDate+"' into end date field");
        }
       
        if (!SeleniumDriverInstance.enterTextByXpath(Audit_PageObjects.newProDateComments(), getData("comments"))) {
            error = "Failed to enter 'New proposed dates comments'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Succsessfully entered  '"+getData("comments")+"' into comments field");
        //Save 
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_SaveToContinue())) {
            error = "Failed to wait for 'Save to continue' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audit_SaveToContinue())) {
            error = "Failed to click 'Save to continue' button.";
            return false;
        }

        if (SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.saveWait(), 2)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Audit_PageObjects.saveWait2(), 400)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }
        narrator.stepPassedWithScreenShot("Succsessfully clicked Save button");
        String[] riskRegisterRecord = SeleniumDriverInstance.retrieveTextByXpath(Audit_PageObjects.getRecord()).split("#");
        setRecordId(riskRegisterRecord[1]);

//        if (!SeleniumDriverInstance.scrollToElement(Audit_PageObjects.auditTeamTab())) {
//            error = "Failed to display 'Audit Team' tab.";
//            return false;
//        }

//        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.riskInciedentTab(), 2)) {
//            error = "Failed to display 'Risk, Obligation & Incidents' tab.";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.actionsTab(), 2)) {
//            error = "Failed to display 'Action' tab.";
//            return false;
//        }

//        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.supportingDocumentTab(), 2)) {
//            error = "Failed to display 'Supporting Documents' tab.";
//            return false;
//        }

//
//        if (!SeleniumDriverInstance.scrollToElement(Audit_PageObjects.auditTeamTab())) {
//            error = "Failed to display 'Audit Team' tab.";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.riskInciedentTab(), 2)) {
//            error = "Failed to display 'Risk, Obligation & Incidents' tab.";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.actionsTab(), 2)) {
//            error = "Failed to display 'Action' tab.";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.supportingDocumentTab(), 2)) {
//            error = "Failed to display 'Supporting Documents' tab.";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.auditFlowProcess())) {
//            error = "Failed to click 'Audit Flow Process' button.";
//            return false;
//        }
        
//        SeleniumDriverInstance.Driver.close();
        
        return true;
    }

}
