/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Audit_TestClasses;




import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Audit_PageObjects.*;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "View Audit Registers - Main Scenario",
        createNewBrowserInstance = false
)

public class FR13_View_Audit_Registers_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    String parentWindow;
    
    public FR13_View_Audit_Registers_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");     
    }

    public TestResult executeTest()
    {
        if (!navigateToAudit())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        if(!enterDetails()){
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully navigated ");
    }

    public boolean navigateToAudit(){
        //Navigate to Environmental Health & Safety
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.navigate_EHS())){
            error = "Failed to wait for 'Environmental, Health & Safety' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.navigate_EHS())){
            error = "Failed to click on 'Environmental, Health & Safety' tab.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Environmental, Health & Safety' tab.");
        
        //Navigate to Stakeholders
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audits_Inspections())){
            error = "Failed to wait for 'Audits & Inspections' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audits_Inspections())){
            error = "Failed to click on 'Audits & Inspections' tab.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Audits & Inspections' tab.");
        
        //Navigate to Stakeholders Entity
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audits())){
            error = "Failed to wait for 'Audits' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audits())){
            error = "Failed to click on 'Audits' tab.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Audits' tab.");

        //Search button
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_search())){
            error = "Failed to wait for 'Search' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audit_search())){
            error = "Failed to click on 'Search' button.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Search' button.");
        parentWindow = SeleniumDriverInstance.Driver.getWindowHandle();
        return true;
    }
    
    //Enter data
    public boolean enterDetails(){
        //Reports
        pause(2000);
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.audit_reports())){
            error = "Failed to wait for 'Reports' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.audit_reports())){
            error = "Failed to click on 'Reports' button.";
            return false;
        }
        //View Reports
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.view_reports())){
            error = "Failed to wait for 'view reports' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.view_reports())){
            error = "Failed to click 'view reports' button.";
            return false;
        }
        //Wait View Reports
        if(!SeleniumDriverInstance.switchToTabOrWindow()){
            error = "Failed to switch to pop-up.";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.popup_conf())){
            error = "Failed to wait for 'Continue' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.popup_conf())){
            error = "Failed to click on 'Continue' button.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully clicked 'Continue' button.");
        
        if(!SeleniumDriverInstance.switchToTabOrWindow()){
            error = "Failed to switch to new tab.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.view_wait(), 50000)){
            error = "Failed to wait for 'View Report' page.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully View Reports.");
        pause(2000);
        SeleniumDriverInstance.switchToDefaultContent();
        SeleniumDriverInstance.Driver.close();
        SeleniumDriverInstance.Driver.switchTo().window(parentWindow);
            
        if (!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.iframeXpath())) {
            error = "Failed to switch to frame ";
            return false;
        }

        if (!SeleniumDriverInstance.switchToFrameByXpath(Audit_PageObjects.iframeXpath())) {
            error = "Failed to switch to frame ";
            return false;
        }
        
        //Full Report
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.full_Reports())){
            error = "Failed to wait for 'Full Reports' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.full_Reports())){
            error = "Failed to select 'Full Reports' button.";
            return false;
        }
        //Wait View Reports
        if(!SeleniumDriverInstance.switchToTabOrWindow()){
            error = "Failed to switch to pop-up.";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.popup_conf())){
            error = "Failed to wait for 'Continue' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.popup_conf())){
            error = "Failed to click on 'Continue' button.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully clicked 'Continue' button.");
        if(!SeleniumDriverInstance.switchToTabOrWindow()){
            error = "Failed to switch to new tab.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.full_wait(), 50000)){
            error = "Failed to wait for 'Full Reports' page.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully Viewed Full Report button.");
        pause(5000);
        return true;
    }

}
