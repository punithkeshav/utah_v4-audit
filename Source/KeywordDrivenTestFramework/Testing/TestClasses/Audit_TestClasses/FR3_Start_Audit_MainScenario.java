/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Audit_TestClasses;


import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Audit_PageObjects.Audit_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Start Audit - Main Scenario",
        createNewBrowserInstance = false
)

public class FR3_Start_Audit_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public FR3_Start_Audit_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {
        if (!startAudit())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        
        
        return narrator.finalizeTest("Completed Capture Audit");
    }
    
    public boolean startAudit(){
        //Selecting the created record
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.startAuditButton())){
            error = "Failed to wait for 'Start Audit' button.";
            return false;
        }
        if(!SeleniumDriverInstance.scrollToElement(Audit_PageObjects.saveButton())){
            error = "Failed to scroll to 'Start Audit' button.";
            return false;
        }
        pause(5000);
        narrator.stepPassedWithScreenShot("Successfully scrolled to 'Start Audit' button.");
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.startAuditButton())){
            error = "Failed to wait for 'Start Audit' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.startAuditButton())){
            error = "Failed to click on 'Start Audit' button.";
            return false;
        }
        if (SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.saveWait(), 2)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Audit_PageObjects.saveWait2(), 400)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }
        pause(5000);
        narrator.stepPassedWithScreenShot("Successfully clicked 'Start Audit' button.");
        
        //validate In Progress Status
//        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.validate_status_InProgress())){
//            error = "Failed to wait for 'Status' dropdown.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.ValidateByText(Audit_PageObjects.validate_status_InProgress(), getData("Status"))){
//            error = "Failed to validate 'Status'.";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Successfully validated 'Status': '" + getData("Status") +"'.");
        
        if(!getData("Sign Off").equalsIgnoreCase("TRUE")){
            SeleniumDriverInstance.Driver.close();
        }

        return true;
    }

}
