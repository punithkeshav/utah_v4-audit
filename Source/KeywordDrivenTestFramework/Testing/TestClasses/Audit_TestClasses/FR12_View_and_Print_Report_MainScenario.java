/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Audit_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import KeywordDrivenTestFramework.Testing.PageObjects.Audit_PageObjects.*;
import io.netty.handler.codec.rtsp.RtspHeaderValues;
import org.joda.time.Minutes;
import org.omg.CORBA.TIMEOUT;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "View and Print Report Main scenario - audit",
        createNewBrowserInstance = false
)

public class FR12_View_and_Print_Report_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public FR12_View_and_Print_Report_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {
        if (!reports()){
            return narrator.testFailed("Failed due - " + error);
        }
        
        return narrator.finalizeTest("Completed navigate to Register Risk");
    }

    public boolean reports(){

        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.reports())){
            error = "Failed to wait for 'Reports' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.reports())){
            error = "Failed to click on 'Reports' button.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully clicked 'Reports' button.");
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.viewAuditReport())){
            error = "Failed to wait for 'Audit Reports' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.viewAuditReport())){
            error = "Failed to click on 'Audit Reports' button.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully clicked 'Audit Reports' button.");
        
        if(!SeleniumDriverInstance.switchToTabOrWindow()){
            error = "Failed to switch to pop-up.";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.popup_conf())){
            error = "Failed to wait for 'Continue' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.popup_conf())){
            error = "Failed to click on 'Continue' button.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully clicked 'Continue' button.");
        
        if(!SeleniumDriverInstance.switchToTabOrWindow()){
            error = "Failed to switch to new tab.";
            return false;
        }
        
        return true;
    }
   
}
