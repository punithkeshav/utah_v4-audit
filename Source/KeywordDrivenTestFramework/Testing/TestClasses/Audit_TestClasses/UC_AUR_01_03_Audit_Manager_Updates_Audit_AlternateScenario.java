/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Audit_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Audit_PageObjects.Audit_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "Audit Manager Updates Audit Alternate Scenario",
        createNewBrowserInstance = false
)

public class UC_AUR_01_03_Audit_Manager_Updates_Audit_AlternateScenario extends BaseClass {

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public UC_AUR_01_03_Audit_Manager_Updates_Audit_AlternateScenario() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");

        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());

    }

    public TestResult executeTest() {
        if (!navigateToAudit()) {
            return narrator.testFailed("Failed due - " + error);
        }
        if (!updateAuditManager()) {
            return narrator.testFailed("Failed due - " + error);
        }

        return narrator.finalizeTest("Completed navigate to Audit and saved record :#" + getRecordId());
    }

    public boolean navigateToAudit(){
        //Navigate to Environmental Health & Safety
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.navigate_EHS())){
            error = "Failed to wait for 'Environmental, Health & Safety' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.navigate_EHS())){
            error = "Failed to click on 'Environmental, Health & Safety' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Environmental, Health & Safety' tab.");
        
        //Navigate to Audit & Inspections
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audits_Inspections())){
            error = "Failed to wait for 'Audit & Inspections.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audits_Inspections())){
            error = "Failed to click on 'Audit & Inspections' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Audit & Inspections' tab.");
       
        //Navigate to Audits
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audits())){
            error = "Failed to wait for 'Audits' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audits())){
            error = "Failed to click on 'Audits' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Audits' tab.");
        
        //Search button
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.SearchButton())){
            error = "Failed to wait for 'Search' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.SearchButton())){
            error = "Failed to click on 'Search' button.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Search' button.");
        
        //Selecting the created record
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.globalCompanyRecord(), 10000)){
            error = "Failed to wait for 'Global Company' record.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.globalCompanyRecord())){
            error = "Failed to click on 'Global Company' record.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click on 'Global Company' record.");

        return true;
    }
    
    public boolean updateAuditManager(){
        //process flow
        pause(5000);
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.processFlowManager(), 10000)){
            error = "Failed to wait for 'Process Flow' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.processFlowManager())){
            error = "Failed to click on 'Process Flow' button.";
            return false;
        }
        //Accept Proposed date
        if(!SeleniumDriverInstance.scrollToElement(Audit_PageObjects.acceptProposedDatesChckbx())){
            error = "Failed to scroll to scheckbox";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.acceptProposedDatesChckbx())){
            error = "Failed to wait for 'Proposed dates' check box.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.acceptProposedDatesChckbx())){
            error = "Failed to click on 'Proposed dates' check box.";
            return false;
        }
        //Save button
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.savenewdatesButton())){
            error = "Failed to wait for 'Submit new dates' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.savenewdatesButton())){
            error = "Failed to click 'Submit new dates' button.";
            return false;
        }
        return true;
    }
    
}
