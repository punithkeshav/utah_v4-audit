/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Audit_TestClasses;


import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Audit_PageObjects.Audit_PageObjects;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Capture Audit Findings Main",
        createNewBrowserInstance = false
)

public class FR4_Capture_Audit_Findings_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public FR4_Capture_Audit_Findings_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {
        if (!IsometrixNavigateSuggestionsAndInnovations())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        
        if(!enterDetails()){
            return narrator.testFailed("Failed due - " + error);
        }
        
        return narrator.finalizeTest("Completed navigate to Audit");
    }

    public boolean IsometrixNavigateSuggestionsAndInnovations(){

        //Navigate to Environmental Health & Safety
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.navigate_EHS())){
            error = "Failed to wait for 'Environmental, Health & Safety' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.navigate_EHS())){
            error = "Failed to click on 'Environmental, Health & Safety' tab.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Environmental, Health & Safety' tab.");
        
        //Navigate to Audit & Inspections
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audits_Inspections())){
            error = "Failed to wait for 'Audit & Inspections.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audits_Inspections())){
            error = "Failed to click on 'Audit & Inspections' tab.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Audit & Inspections' tab.");
       
        //Navigate to Audits
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audits())){
            error = "Failed to wait for 'Audits.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audits())){
            error = "Failed to click on 'Audits' tab.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Audits' tab.");
        //Search button
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Audit_search())){
            error = "Failed to wait for 'Search' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Audit_search())){
            error = "Failed to click on 'Search' button.";
            return false;
        }
        SeleniumDriverInstance.pause(2000);
        narrator.stepPassedWithScreenShot("Successfully click 'Search' button.");
        //Record Select
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.globalCompanyRecord())){
            error = "Failed to wait for 'Global Company' record.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.globalCompanyRecord())){
            error = "Failed to click on 'Global Company' record.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Global Company' record.");
        //Findings tab
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.Af_tab())){
            error = "Failed to wait for 'Findings' tab";
            return false;
        }
        pause(2000);
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.Af_tab())){
            error = "Failed to click on 'Findings' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Findings' tab");
        //Add button
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.af_add())){
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.af_add())){
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");
        return true;
    }
    
    //Enter data
    public boolean enterDetails(){
        //Process Flow
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.af_processflow())){
            error = "Failed to wait for Process Flow button.";
            return false;
        } 
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.af_processflow())){
            error = "Failed to click Process Flow button.";
            return false;
        }
        //Elements dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.af_elements_dropdown())){
            error = "Failed to wait for Audit Elements dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.af_elements_dropdown())){
            error = "Failed to click Audit Elements dropdown.";
            return false;
        }
        //Elements Select
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.af_elements_select(testData.getData("Audit Elements")))){
            error = "Failed to wait for '"+testData.getData("Audit Elements")+"' in Audit Elements dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.af_elements_select(testData.getData("Audit Elements")))){
            error = "Failed to click '"+testData.getData("Audit Elements")+"' from Audit Elements dropdown.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Audit Elements entered.");
        //Findings Description
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.af_desc())){
            error = "Failed to wait for Description textarea.";
            return false;
        }   
        if(!SeleniumDriverInstance.enterTextByXpath(Audit_PageObjects.af_desc(), testData.getData("Description"))){
            error = "Failed to enter text into Description textarea.";
            return false;
        }
        //Findings Owner dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.af_owner_dropdown())){
            error = "Failed to wait for Findings Owner dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.af_owner_dropdown())){
            error = "Failed to click Findings Owner dropdown.";
            return false;
        }
        //Findings Owner select
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.af_owner_select(testData.getData("Findings Owner")))){
            error = "Failed to wait for '"+testData.getData("Findings Owner")+"' in Findings Owner dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.af_owner_select(testData.getData("Findings Owner")))){
            error = "Failed to click '"+testData.getData("Findings Owner")+"' from Findings Owner dropdown.";
            return false;
        }
        //Risk Source dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.af_risksource_dropdown())){
            error = "Failed to wait for Risk source dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.af_risksource_dropdown())){
            error = "Failed to click Risk source dropdown.";
            return false;
        }
        //Risk Source select option 1
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.af_risksource_select(testData.getData("Risk Source 1")))){
            error = "Failed to wait for '"+testData.getData("Risk Source 1")+"' in Risk Source dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.af_risksource_select(testData.getData("Risk Source 1")))){
            error = "Failed to click '"+testData.getData("Risk Source 1")+"' from Risk Source dropdown.";
            return false;
        }
        //Risk Source select option 2
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.af_risksource_select2(testData.getData("Risk Source 2")))){
            error = "Failed to wait for '"+testData.getData("Risk Source 2")+"' in Risk Source dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.af_risksource_select2(testData.getData("Risk Source 2")))){
            error = "Failed to click '"+testData.getData("Risk Source 2")+"' from Risk Source dropdown.";
            return false;
        }
        //Risk Source arrow
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.af_risksource_arrow())){
            error = "Failed to wait for Risk source arrow.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.af_risksource_arrow())){
            error = "Failed to click Risk source arrow.";
            return false;
        }
        //Findings Classification dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.af_class_dropdown())){
            error = "Failed to wait for Findings Classification dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.af_class_dropdown())){
            error = "Failed to click Findings Classification dropdown.";
            return false;
        }
        //Findings Classification select
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.af_class_select(testData.getData("Findings Classification")))){
            error = "Failed to wait for '"+testData.getData("Findings Classification")+"' in Findings Classification dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.af_class_select(testData.getData("Findings Classification")))){
            error = "Failed to click '"+testData.getData("Findings Classification")+"' from Findings Classification dropdown.";
            return false;
        }
        //Risk dropdown
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.af_risk_dropdown())){
            error = "Failed to wait for Risk source dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.af_risk_dropdown())){
            error = "Failed to click Risk source dropdown.";
            return false;
        }
        //Risk select
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.af_risk_select(testData.getData("Risk")))){
            error = "Failed to wait for '"+testData.getData("Risk")+"' in Risk dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.af_risk_select(testData.getData("Risk")))){
            error = "Failed to click '"+testData.getData("Risk")+"' from Risk dropdown.";
            return false;
        }
        //Save to continue
        if(!SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.af_stc())){
            error = "Failed to wait for 'Save to continue' button.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(Audit_PageObjects.af_stc())){
            error = "Failed to click 'Save to continue' button.";
            return false;
        }
        //Save mask
        if (SeleniumDriverInstance.waitForElementByXpath(Audit_PageObjects.saveWait(), 2)) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Audit_PageObjects.saveWait2(), 400)) {
                error = "Webside too long to load wait reached the time out";
                return false;
            }
        }
        pause(5000);
        return true;
    }

}
