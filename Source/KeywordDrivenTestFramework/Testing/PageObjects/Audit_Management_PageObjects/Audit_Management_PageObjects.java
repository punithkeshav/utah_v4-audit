/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Audit_Management_PageObjects;

/**
 *
 * @author SMABE
 */
public class Audit_Management_PageObjects
{

    public static String Record_Number;
    public static String Window_1;

    public static String getWindow()
    {
        return Window_1;
    }

    public static void setWindow(String Window)
    {
        Window_1 = Window;
    }

    public static String getRecord_Number()
    {
        return Record_Number;
    }

    public static void setRecord_Number(String Record_Number_)
    {
        Record_Number = Record_Number_;
    }

    public static String Text1(String text)
    {
        return "/html/body/div[1]/div[3]/div/div[2]/div[33]/ul[1]/ul/li/a";
    }

    public static String RecordDelete()
    {
        return "//div[@id='btnDelete_form_1B0044DE-2C55-488C-9850-75C417EED9C8']";
    }

    public static String CloseCurrentModule()
    {
        return "(//div[contains(@class,'form active transition visible')]//div[@class='navbar']//i[@class='close icon cross'])[1]";
    }

    public static String CloseCurrentModule2()
    {
        return "(//div[contains(@class,'form transition visible active')]//div[@class='navbar']//i[@class='close icon cross'])[1]";
    }

    public static String DeleteButton()
    {
        return "//div[text()='Delete']";
    }

    public static String Record(String string)
    {
        return "//span[text()='" + string + "']";
    }

     public static String businessUnitOption1(String text)
    {
        return "(//div[contains(@class,'transition visible')]//a[text()='" + text + "']/..//i)[1]";
    }
    public static String ButtonConfirm()
    {
        return "//div[@id='btnConfirmYes']";
    }

    public static String Text3(String text)
    {
        return "(//a[contains(text(),'" + text + "')])/../..//li//a[contains(text(),'" + text + "')]";

    }

    public static String ButtonOK()
    {
        return "//div[contains(@class,'centreTopPopup transition visible')]//div[text()='OK']";
    }

    public static String AuditorTypeDropDowm()
    {

        return "//div[@id='control_F8F35C1B-6B3C-4885-9834-C64C5DDA4555']";
    }

    public static String Text2(String text)
    {
        return "//div[contains(@class,'transition visible')]//a[contains(text(),'" + text + "')]";

    }

    public static String Text(String text)
    {
        return "//a[text()='" + text + "']";

    }

    public static String projectTitle()
    {
        return "IsoMetrix";
    }

    public static String Username()
    {
        return "//input[@id='txtUsername']";
    }

    public static String AuditManagementLabel()
    {
        return "//label[text()='Audit Management']";
    }

    public static String RecordLabel()
    {
        return "(//div[@id='divPager']//div[contains(text(),'Record')])[1]";
    }

    public static String AuditManagementLabel_1()
    {
        return "(//div[text()='Audit Management'])[1]";
    }

    public static String Actions_add()
    {
        return "//div[@id='btnActAddNew']";
    }

    public static String VerificationAdd()
    {
        return "//div[@id='control_EDE28D9B-722E-4786-8CCB-FFA228880432']//div[text()='Add']";
    }

    public static String ContainsTextBox()
    {
        return "//input[@class='txt border']";
    }

    public static String VerifyDropDown()
    {
        return "//div[@id='control_AC075E37-7F8F-4DEC-8DF0-02DC8D70BDFB']//ul";

    }

    public static String BusinessUnitDropDown()
    {
        return "//div[@id='control_B1ED7656-E00D-4A2E-B3FF-93E068F7D531']//ul";

    }

    public static String FunctionLocation()
    {
        return "//div[@id='control_40E9FB65-C2EA-45A7-8497-24D8EA2CC6AE']//ul";
    }

    public static String AuditTitle()
    {
        return "(//div[@id='control_FEB3D889-9079-4781-943C-D2CA02A5A5F4']//input)[1]";
    }

    public static String AuditScope()
    {
        return "//div[@id='control_9C956F79-A9C9-470F-975D-4923F2DB6E7A']//textarea";
    }

    public static String LinkProjectCheckBox()
    {
        return "//input[@id='Linktoproject']/..";
    }

    public static String ProjectDropDown()
    {
        return "//div[@id='control_6380EE87-B589-48A4-BE47-2B255AA00E5C']//ul";
    }

    public static String AuditTypeDropDown()
    {
        return "//div[@id='control_2C70ED4C-D256-4B45-B76F-7E916644A513']//ul";
    }

    public static String RelatedStakeholderDropDown()
    {
        return "//div[@id='control_5E74E964-4873-4B1D-8162-4EE4532A3FE8']/..//span[@class='select3-arrow']";
    }

    public static String AuditManagerDropDown()
    {
        return "//div[@id='control_8F83D7C1-3D82-47CE-8A8A-BCB3A0A5248E']//ul";
    }

    public static String AuditeeDropDown()
    {
        return "//div[@id='control_CCD4D19C-9ADA-49A2-9824-9BFD60753453']//ul";
    }

    public static String AudiStartDate()
    {
        return "//div[@id='control_C58B4570-0BCC-4016-955F-BC39F5A7CBD9']//input";
    }

    public static String AudiEndDate()
    {
        return "//div[@id='control_155B5250-AFF6-4EE7-A2B2-200427F133C8']//input";
    }

    public static String ButtonSave()
    {
        return "//div[@id='btnSave_form_1B0044DE-2C55-488C-9850-75C417EED9C8']";
    }

    public static String TextCheckBox(String text)
    {

        return "//a[text()='" + text + "']//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String ProcessActivity()
    {
        return "//div[@id='control_6B5F3505-F436-4ED1-B8B1-38D8DB086FC2']//span[@role='presentation']";
    }

    public static String processFlow()
    {
        return "//div[@id='btnProcessFlow_form_1B0044DE-2C55-488C-9850-75C417EED9C8']";
    }

    public static String SupportingTab()
    {
        return "//li[@id='tab_7960880A-D0C6-4353-832D-D46245B6D262']";
    }

    public static String VerificationTab()
    {
        return "//div[text()='Verification']";
    }

    public static String linkADocument()
    {
        return "//b[@class='linkbox-link']";
    }

    public static String RelatedStakeholderTextBox()
    {
        return "//div[@id='select3_b7ccd927']//input";
    }

    public static String AuditeeDropDownTextBox()
    {
        return "//div[@id='select3_da7f05de']//input";
    }

    public static String AuditManagerDropDownTextBox()
    {
        return "//div[@id='select3_ce9e4e25']//input";
    }

    public static String TypeSearch()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type and enter to search']";
    }
   
     public static String BusinessUnitexpandButton()
    {
        return "//div[contains(@class,'transition visible')]//i[@class='jstree-icon jstree-ocl']";
    }
    
     public static String TypeSearch2()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type to search']";
    }
    public static String TypeSearch_2()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type to search']";
    }

    public static String AuditemplateDropDown()
    {
        return "//div[@id='control_2C3B9625-8F0F-44C8-8CB9-8C6600FB57E0']//ul"; 
   }

    public static String AudiTemplateType(String text)
    {
        return "//div[contains(@class,'transition visible')]//a[text()='" + text + "']";
    }

    public static String recordSaved_popup()
    {
        return "//div[contains(@class,'transition visible')]//div[@id='txtHeader'][contains(text(),'Record saved')]";
    }

    public static String recordSaved_popup_2()
    {
        return "//div[@class='ui floating icon message transition visible']//div[@id='txtHeader'][contains(text(),'Record saved')]";
    }

    public static String failed()
    {
        return "//div[contains(@class,'transition visible')]//div[@id='txtHeader'][contains(text(),'Record has no changes to save')]";
    }

    public static String Savewait()
    {
        return "(//div[text()='Action Feedback'])[1]";
    }

    public static String getActionRecord()
    {
        return "//div[@class='record']";
    }

    public static String SubmitAudit()
    {
        return "//div[contains(@class,'transition visible')]//div[text()='Submit audit']";
    }

    public static String supporting_tab()
    {
        return "//div[text()='Supporting Documents']";
    }

    public static String AuditDetailsTab()
    {
        return "//div[text()='Audit Details']";
    }

    public static String AuditProtocolTab()
    {
        return "(//div[text()='Audit Protocol'])";
    }

    public static String UrlInput_TextArea()
    {
        return "//input[@id='urlValue']";
    }

    public static String tile_TextArea()
    {
        return "//input[@id='urlTitle']";
    }

    public static String linkADoc_Add_button()
    {
        return "//div[@id='btnConfirmYes']//div[text()='Add']";
    }

    public static String iframeXpath()
    {
        return "//iframe[@id='ifrMain']";
    }

    public static String ScheduleApprovedCheckBox()
    {
        return "//div[@id='control_5467B726-978A-457D-BE3C-6A1BCAA9DDAD']//input/../..//div";
    }

    public static String AuditManagementSearch()
    {
        return "//div[text()='Search']/../.";
    }

    public static String AuditManagementSearchResults()
    {
        return "//tbody[@role='rowgroup']";
    }

    public static String AuditManagementSearchRecord(String text)
    {

        return "(//div[text()='" + text + "'])[1]";
    }

    public static String SubmitAuditPlanningButton()
    {
        return "(//div[text()='Submit audit planning'])[1]";
    }

    public static String LoadingCircle()
    {
        return "//div[@class='k-loading-image']";

    }

    public static String ApplyFilterButton()
    {
        return "//div[@id='btnActApplyFilter']";
    }

    public static String LoadingData()
    {
        return "//div[text()='Loading checklist summary']";

    }

    public static String ProposeNewDatesCheckBox()
    {

        return "//div[@id='control_700E32B0-1735-45D2-BAC4-DD98C85091DF']";
    }

    public static String SuggestedStartDate()
    {
        return "(//div[text()='Suggested start date']/../..//input[@data-role='datepicker'])[1]";
    }

    public static String SuggestedEndDate()
    {
        return "(//div[text()='Suggested start date']/../..//input[@data-role='datepicker'])[2]";
    }

    public static String Comments()
    {
        return "//div[text()='New proposed dates comments']/../..//textarea";
    }

    public static String NewProposedDatesComments()
    {
        return "//div[@id='control_BC062ECD-9E04-40C3-8EF4-0B4007B9DEB0']//textarea";
    }

    public static String AuditComments()
    {
        return "//*[@id=\"control_5FC74CF8-0BE4-459D-9599-A35927F43D28\"]/div[1]/div/textarea";
    }

    public static String EmailCommentsTo()
    {
        return "//div[@id='control_BB2685EF-0F6D-42D2-9323-51F69811682A']//ul";
    }

    public static String AcceptProposedDatesCheckBox()
    {
        return "//div[@id='control_30FEA922-FA7C-4572-A387-44875026F2A3']";
    }

    public static String SubmitNewDatesButton()
    {
        return "//div[@id='control_27CA5F1A-F9FA-4FD4-8F42-ACF7DBCF0DB7']";
    }

    public static String AuditTeamTab()
    {
        return "(//div[text()='Audit Team'])[1]";
    }

    public static String AuditTeamAdd()
    {
        return "(//div[@id='btnAddNew'])[1]";
    }

    public static String FullNameDropDowm()
    {
        return "//div[@id='control_E0535A21-FBC8-4F90-B767-B9C02D121842']";

    }

    public static String AuditorDropDowm()
    {
        return "//div[@id='control_DEA38CFE-258E-4507-BE92-180D8536EF89']//ul";
    }

    public static String AuditorDropDowmText()
    {

        return "//*[@id=\"select3_cc8401fc\"]/div[1]/input";
    }

    public static String ExperienceRoleDropDowm()
    {
        return "//div[@id='control_A1BF9E59-A685-4589-A3B6-9519333C11D1']";
    }

    public static String EventsTab()
    {
        return "//div[text()='Events']";
    }

    public static String RisksAndIncidentsTab()
    {
        return "//div[text()='Risks and Incidents']";
    }

    public static String EventManagementTab()
    {
        return "//div[text()='Event Management']";
    }

    public static String EventsRecord()
    {
        return "//div[@id='control_E2DA37B7-70A9-4AE4-959F-14FC8AF0F364']//tbody[@role='rowgroup']//tr[1]";
    }

    public static String RecordProccessFlow()
    {
        return "//div[@id='btnProcessFlow_form_B6196CB4-4610-463D-9D54-7B18E614025F']//span";
    }

    public static String AuditProccessFlow()
    {
        return "//div[@id=btnProcessFlow_form_C9BF4B41-F36D-48FC-8F3F-FE6BC39DEE07']";
    }

    public static String CloseRecord()
    {
        return "(//div[@id='divForms']//i[@class='close icon cross'])[1]";
    }

    public static String CloseRecord_2()
    {
        return "(//div[contains(@class,'form active transition hidden')]/..//i[@class='close icon cross'])[1]";
    }

    public static String CloseRecord_3()
    {
        return "//div[contains(@class,'form transition visible active')]//i[@class='close icon cross']";
    }

    public static String CloseRecord_4()
    {
        return "//div[contains(@class,'form active transition visible')]//i[@class='close icon cross']";
    }

    public static String StatusDropDown()
    {
        return "(//li[text()='Please select'])[1]";
    }

    public static String StatusDropDownOption(String text)
    {
        return "//div[contains(@class,'transition visible')]//a[@class='jstree-anchor'][contains(text(),'" + text + "')]//i[1]";
    }

    public static String SearchButton()
    {
        return "//div[@id='btnActApplyFilter']";
    }

    public static String StartAuditButton()
    {
        return "//div[text()='Start audit']";
    }

    public static String RequestVerficationButton()
    {
        return "//div[text()='Request verification']";
    }

    public static String AuditDetailStatusDD(String text)
    {
        return "//*[@id=\"control_E8EBE770-3C9A-4EB6-93A4-35BF3D60C7EF\"]/div[1]/a";
    }

    public static String StartButton()
    {
        return "//div[contains(text(),'START')]";
    }

    public static String PersonConductingTheAuditDropDown()
    {
        return "//div[@id='control_DC3EE96D-3BB3-4B26-8D65-33EA527EFA79']//ul";
    }

    public static String AuditProtocolAutoTab()
    {
        return "//div[text()='Audit Protocol Auto']";
    }

    public static String Question1DropDown()
    {
        return "//div[@id='control_C070EEE6-28B2-4429-8CE9-4438185EC1A7']";
    }

    public static String SaveCurrentSection()
    {
        return "//div[text()='Save current section']";
    }

    public static String AuditManagementClose()
    {
        return "//div[@id='divPage']//div[contains(@class,'form transition visible active')]//i[@class='close icon cross']";
        //return"//div[contains(@class,'form transition visible active')]//div[@class='navbar']//i[2]";           
    }

    public static String ContinueButton()
    {
        return "//div[contains(text(),'Continue')]";
    }

    public static String FindingsTab()
    {

        return "//div[text()='Findings']";
    }

    public static String TableResults()

    {
        return "(//div[text()='No results returned'])[1]";
    }

    public static String Comment()
    {
        return "(//div[@id='control_D2279746-0960-4424-8238-125B42AD00A5']//input)[1]";
    }

    public static String PreviousButton()
    {
        return "//div[contains(text(),'Previous')]";
    }

    public static String NextButton()
    {
        return "//div[contains(text(),'Next')]";
    }

    public static String cross()
    {
        return "Finale.PNG";
    }

    public static String Yes()
    {
        return "Yes.PNG";
    }

    public static String cross2()
    {

        return "AAA.PNG";
        // return"cancel.PNG";

    }

    public static String SubModules()
    {
        return "//i[@id='control_minimized_submodule_C070EEE6-28B2-4429-8CE9-4438185EC1A7']";
    }

    public static String AuditManagementFindingLabel()
    {
        return "(//div[text()='Audit Management Finding'])[2]";
    }

    public static String AuditManagementFindingAdd()
    {
        return "(//div[text()='Audit Management Finding'])[2]/..//div[@id='btnAddNew']";
    }

    public static String FindingDescription()
    {
        return "//div[@id='control_40ECC722-B08B-48F3-9906-3CFCE527C5CD']//textarea";
    }

    public static String FunctionalDepartmentDropDown()
    {
        return "//div[@id='control_DE18F9A3-BBB4-4600-B420-0167AE11B426']";
    }

    public static String FindingOwnerDropDown()
    {
        return "//div[@id='control_434D9128-5EBC-4E25-9836-72A2C4451733']";
    }

    public static String FindingClassificationDropDown()
    {

        return "//div[@id='control_7689E71C-DC7E-41C1-9124-C6312596E956']";
    }

    public static String RiskSourceDropDown()
    {
        return "//div[@id='control_E2CBB706-26AB-4373-813A-05D4860FC38F']";
    }

    public static String RiskSourceSelectAll()
    {
        return "//div[@id='control_E2CBB706-26AB-4373-813A-05D4860FC38F']//b[@original-title='Select all']";
    }

    public static String FindindsButtonSave()
    {
        return "//div[@id='btnSave_form_D0DC2467-E0D2-4AB2-BC47-B8987BA93E91']";
    }

    public static String FindingsClose()
    {
        return "findingsClose.PNG";
    }

    public static String ProcessActivitySelectAll()
    {
        return "//div[@id='control_6B5F3505-F436-4ED1-B8B1-38D8DB086FC2']//span//b[@class='select3-all']";
    }

    public static String RecordHeading(String text)
    {
        return "(//div[contains(text(),'" + text + "')])[1]";
    }

    public static String EventManagementHeading()
    {
        return "(//div[text()='Event Management'])[1]";
    }

    public static String Text_2(String text)
    {
        return "//div[contains(@class,'transition visible')]//a[text()='" + text + "']";

    }

    public static String AudiTemplateOption(String data)
    {
        return "//li[@title='" + data + "']";
    }

    public static String SupportTab()
    {
        return "//div[contains(text(),'10. Support')]";
    }

    public static String Answer_1_DropDown()
    {
        return "//div[@id='control_4600E8BA-2DDD-4EA1-A142-52404A872910']//ul";
    }

    public static String LoadingDataLoader()
    {
        return "//div[contains(@class,'form transition visible active')]//div[@id='txtWait']";
    }

    public static String AuditCross()
    {
        return "Audit.png";
    }

    public static String ImpactTypeDropDown()
    {
        return "//div[@id='control_7333CA9A-96BE-486A-B8F4-DC07A0D58268']//ul";
    }

    public static String ImpactTypeDropDownOption(String data)
    {
        return "//div[contains(@class,'transition visible')]//a[text()='" + data + "']//i[1]";
    }

    public static String AuditButtonSave()
    {
        return "//div[contains(@class,'form active transition visible')]//div[text()='Save']";
    }

    public static String AuditObjective()
    {
        return "//div[@id='control_EE073962-0DB4-4FD6-B88F-95B13A10D358']//textarea";
    }

    public static String IntroductionInput()
    {
        return "//div[@id='control_9AAF2CD3-5F45-4560-962B-1E4A39280565']//textarea";
    }

    public static String RelatedEventsDropDown()
    {
        return "//span[text()='Related Events']/..//i";
    }

    public static String FunctionalLocationDropDown()
    {
        return "//div[@id='control_40E9FB65-C2EA-45A7-8497-24D8EA2CC6AE']";
    }

    public static String AuditType(String data)
    {
        return "(//div[contains(@class,'transition visible')]//a[text()='" + data + "']/..//i)[2]";
    }

    public static String TabsTotal()
    {
        return "//div[contains(@class,'form active transition visible')]//div[@class='tabpanel_move_content tabpanel_move_content_scroll']//li";
    }

    public static String FinishButton()
    {
        return "//div[text()='Finish']";
    }

    public static String VerificationComments()
    {
        return "//div[@id='control_5FC74CF8-0BE4-459D-9599-A35927F43D28']//textarea[@class='txt translatable']";
    }

    public static String TypeSearchCheckBox(String data)
    {
        return "//div[contains(@class,'form active transition hidden')]/..//a[text()='" + data + "']/.//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String VerificationProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_2A91B334-A8CC-4FD6-8D7D-01BC35649F02']";
    }

    public static String VerifiySaveButton()
    {
        return "//div[@id='btnSave_form_2A91B334-A8CC-4FD6-8D7D-01BC35649F02']";
    }

    public static String RisksTab()
    {
        return "//div[text()='Events']";
    }

    public static String AddAuditManagementFinding()
    {
        return "(//div[@id='control_49cc21ae-0c40-44f1-aeb6-9e1c08ba43bb']//tr[2]//i)[2]";
    }

    public static String Audit_Management_Finding_Add_Button()
    {
        return "//div[@id='control_38C64463-952D-4E0F-BBDD-9B9B87DE03F5']//div[@id='btnAddNew']";
    }

    public static String FindingsProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_D0DC2467-E0D2-4AB2-BC47-B8987BA93E91']";
    }

    public static String FindindsButtonSaveDropDown()
    {
        return "//div[@id='btnSave_form_D0DC2467-E0D2-4AB2-BC47-B8987BA93E91']//div[@class='more options icon chevron down']";
    }

    public static String FindindsButtonSaveAndClose()
    {
        return "//div[@id='btnSaveClose_form_D0DC2467-E0D2-4AB2-BC47-B8987BA93E91']//div[text()='Save and close']";
    }

    public static String FindingsRecord()
    {
        return "(//div[@id='control_38C64463-952D-4E0F-BBDD-9B9B87DE03F5']//div[@class='bottom']//table)[2]//tr[1]";
               // (//div[@id='control_38C64463-952D-4E0F-BBDD-9B9B87DE03F5']//table//tbody)[2]//tr
    }

    public static String SubmitAuditPlanning()
    {
        return "//div[text()='Submit audit planning']";
    }

    public static String StartButton_2()
    {
        return"//div[@id='btnChecklist_form_1B0044DE-2C55-488C-9850-75C417EED9C8']";
    }

    public static String AddFinding()
    {
        return"(//i[@original-title='Add/edit sub-module records'])[1]";
    }

    public static String Audit_Management_Finding_Close_Button()
    {
        return"(//div[text()='Sub modules']/../..//i)[1]";
    }

    public static String AudiTemplateTypeDropDown(String data)
    {
        return"(//a[text()='"+data+"']/..//i)[1]";
    }
}
