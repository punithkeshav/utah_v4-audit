/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Audit_PageObjects;

import KeywordDrivenTestFramework.Testing.PageObjects.Audit_PageObjects.*;
import KeywordDrivenTestFramework.Core.BaseClass;

/**
 *
 * @author syotsi
 */
public class Audit_PageObjects extends BaseClass {

    
    
    public static String MaskBlock() {
return"//div[@class='ui inverted dimmer active']";
}

public static String MaskNone() {
return"//div[@class='ui inverted dimmer']";
}
     public static String viewAuditReport(){
        return "(//span[text()='Audit Report']/../span[2])[3]";
    }
     
      public static String reports(){
        return "//div[@id='btnReports_form_C9BF4B41-F36D-48FC-8F3F-FE6BC39DEE07']";
    }
    public static String BPM_add()
    {
        return "//div[@id='btnConfirmYes']";
    }
     public static String title_field()
    {
        return "//input[@id='urlTitle']";
    }
    public static String url_field()
    {
        return "//input[@id='urlValue']";
    }
    public static String processFlow;

    //Navigate to Suggestions and Innovations
    public static String navigate_EHS() {
        return "//label[contains(text(),'Environmental, Health & Safety')]";
    }

    public static String Audits_Inspections() {
        return "//label[text()='Audits & Inspections']";
    }

    public static String Audits() {
        return "//label[text()='Audits']";
    }

    //FR1 Capture Audit
    public static String Audit_Add() {
        return "//div[text()='Add']";
    }

    public static String Audit_BI_dropdown() {
        return "//div[@id='control_5B60ABCF-0B9E-4AF6-B604-8B8C45008B63']";
    }

    public static String Audit_BU_select(String text){
        return "//a[text()='"+text+"']";
    }
    public static String Audit_BU_expand(String text){
        return "//a[text()='"+text+"']/../i";
    }

    public static String Audit_Projectlink_checkbox() {
        return "//div[@id='control_D5B5BDBE-CBBF-47CD-942C-B9D6ABE9CE60']";
        //input[@type='checkbox']
    }

    public static String Audit_Project_dropdown() {
        return "//div[@id='control_5F3EB691-4CF3-4CA2-891E-734354744DD4']";
    }

    public static String Audit_Project_select(String text) {
        return "//div[@id='divForms']/div[6]//a[text()='" + text + "']";
    }

    public static String Audit_ImpactType_dropdown() {
        return "//div[@id='control_7333CA9A-96BE-486A-B8F4-DC07A0D58268']";
    }

    public static String Audit_ImpactType_selectall() {
        return "//div[@id='control_7333CA9A-96BE-486A-B8F4-DC07A0D58268']//b[@original-title='Select all']";
    }

    public static String Audit_ImpactType_select(String text) {
        return "//div[@id='divForms']/div[7]//a[text()='" + text + "']/i[1]";
    }

    public static String Audit_ImpactType_hardwait() {
        return "//div[@id='divForms']/div[7]//a[text()='Environmental']/i[1]";
    }

    public static String Audit_title() {

        return "//div[@id='control_190408A8-3D41-4334-A66F-DE1006C92980']/div/div/input";
    }

    public static String Audit_Scope() {
        return "//div[@id='control_04975551-BEE4-4278-9E47-77224B6CA1AA']//textarea";
    }

    public static String Audit_Type_dropdown() {
        return "//div[@id='control_762578C8-DE68-4025-B029-4FC1FAB832B5']";
    }

    public static String Audit_Type_select(String text) {
        return "//div[@id='divForms']/div[8]//a[text()='" + text + "']";
    }

    public static String Audit_Type_select2(String text) {
        return "//div[@id='divForms']/div[8]//a[text()='" + text + " ']";
    }

    public static String Audit_Protocol_dropdown() {

        return "//div[@id='control_464907F1-B84F-4A4F-8228-7BDAF119EE53']";
    }

    public static String Audit_Protocol_Select(String text) {
        return "//div[@id='divForms']/div[10]//a[text()='" + text + "']";
    }

    public static String Audit_RS_dropdown() {
        return "//div[@id='control_74BC552C-C07A-486B-AC3F-1174C339C5BE']//b[1]";

    }

    public static String Audit_RS_select(String text) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'"+text+"')]";
    }

    public static String Audit_PA_selectall() {
        return "//div[@id='control_EEA456A2-6A09-4F8B-85AB-C2F3E0581B03']//b[@original-title='Select all']";
    }

    public static String Audit_PA_select(String text) {
        return "//div[@id='control_EEA456A2-6A09-4F8B-85AB-C2F3E0581B03']//a[text()='" + text + "']/i[1]";
    }

    public static String Audit_manager_dropdown() {
        return "//div[@id='control_D718214C-2A59-46A6-A6E8-7F5FDFBBF85F']";
    }

    public static String Audit_manager_select(String text) {
        return "//div[@id='divForms']/div[11]//a[contains(text(),'" + text + "')]";
    }

    public static String Audit_Auditee_dropdown() {
        return "//div[@id='control_22098F99-2305-40B1-85C3-860634B5B5F1']";
    }

    public static String Audit_Auditee_select(String text) {
        return "//div[@id='divForms']/div[12]//a[contains(text(),'" + text + "')]";
    }

    public static String Audit_Person_dropdown() {
        return "//div[@id='control_44BFB53D-90E8-4FED-84C4-3C4C4A6A1F46']";
    }

    public static String Audit_Person_select(String text) {
        return "//div[@id='divForms']/div[13]//a[contains(text(),'" + text + "')]";
    }

    public static String Audit_startdate() {
        return "//div[@id='control_40752CA7-D6F1-429B-BF6E-ADD0A20A8FC0']//input";
    }

    public static String Audit_enddate() {
        return "//div[@id='control_4ACB9871-78C2-4168-B78A-CCF6CD3FB877']//input";
    }

    public static String Audit_IntroTab() {
        return "//span[text()='Introduction & Audit Objective']";
    }

    public static String Audit_Intro_textarea() {
        return "//div[@id='control_5550C644-1C46-4904-A7F7-01648A0B3674']//textarea";
    }

    public static String Audit_Obj_textarea() {
        return "//div[@id='control_C559A407-5194-4245-83E3-7EDDB7598A8E']//textarea";
    }

    //Save Buttons
    public static String Audit_Submit() {
        return "//div[@id='control_1F8CB22C-C147-416C-88BD-139A6744FAA8']";
    }

    public static String Audit_Save() {
        return "//div[@id='divForms']/div[2]/div[2]//div[text()='Save']";
    }

    public static String Audit_SaveToContinue() {
        return "//div[@class='form active transition visible']//div[@title='Save']";
    }

    public static String proposeNewDatesCheckbox() {
        return "//div[@id='control_D8D6E5B8-56D4-4FBC-8723-D67F071BA164']";
    }

    public static String suggestedStartDate() {
        return "//div[@id='control_316CCF4D-814A-4538-BA5A-E9245C8FF01C']//input";
    }

    public static String suggestedEndDate() {
        return "//div[@id='control_112EEFA2-F98A-445B-A27F-D0C0EE00008F']//input";
    }

    public static String newProDateComments() {
        return "//div[@id='control_1CB03513-F3F5-41DA-A672-E27CD1BB069C']//textarea";
    }

    public static String auditFlowProcess() {
        return "//div[@id='btnProcessFlow_form_C9BF4B41-F36D-48FC-8F3F-FE6BC39DEE07']";
    }

    public static String saveWait() {
        return "//div[@class='ui inverted dimmer active']";
    }

    public static String saveWait2() {
        return "//div[@class='ui inverted dimmer']/div[text()='Saving...']";

    }

    public static String auditTeamTab() {
        return "//li[@id='tab_6DB1004F-AA3F-4790-8C97-0B42EC4290D0']";
    }

    public static String riskInciedentTab() {
        return "//li[@id='tab_156B06A5-B098-4DE8-8E0B-92A0F081C4CA']";
    }

    public static String actionsTab() {
        return "//li[@id='tab_0035500A-7FB8-4C70-A02D-297760250592']";
    }

    public static String EnvironmentalOption() {
        return "//div[@id='divForms']/div[7]//a[contains(text(),'Environmental')]";
    }
    
    

    public static String supportDocument() {
        return "//li[@id='tab_B198706E-E62D-4EE0-B919-DDB49D90ABAD']//div[contains(text(),'Supporting Documents')]";
    }

    public static String supportingDocumentTab() {
        return "//div[text()='Supporting Documents']";
    }

    public static String getRecord() {
        return "//div[@class='recnum']//div[@class='record']";
    }
    //FR2 Capture Audit Team
    public static String Audit_TeamTab() {
        return "//div[@id='control_6D1637B8-FFA4-4A93-B56F-0CEF65B47FE6']/div[9]/div/div[3]//div[text()='Audit Team']";
    }
    public static String SearchButton(){
        return "//div[@class='actionBar filter']/div[1]//div[contains(text(),'Search')]";
    }
    public static String globalCompanyRecord(){
        return "(//div[text()='Global Company'])[1]";
    }
    public static String acceptProposedDatesChckbx(){
        return "//div[@id='control_D32D88DD-06DF-4A16-85A3-D64D955BDAE2']";
    }
    
    public static String processFlowManager(){
        return "//div[@id='btnProcessFlow_form_C9BF4B41-F36D-48FC-8F3F-FE6BC39DEE07']";
    }
    
    

    public static String savenewdatesButton() {
        return "//div[text()='Submit New Dates']";
    }
    
    public static String saveButton(){
        return "(//div[text()='Save'])[2]";
    }
    
    public static String relatedRiskLinkedToBusiness(){
        return "//span[contains(text(),'Related Risk Linked to Business Unit, Impact type and Process/Activity')]";
    }
    
    public static String Audit_Team_ProcessFlow(){
        return "//div[@id='btnProcessFlow_form_C9BF4B41-F36D-48FC-8F3F-FE6BC39DEE07']";
    }
    
    public static String Audit_Team_Add(){
        return "(//div[text()='Add'])[2]";
    }
    
    public static String Audit_Team_Fullname_dropdown(){
        return "//div[@id='control_74F3ABB3-D328-4733-BFC4-9D151C8478D3']//li";
    }
    
    public static String Audit_Team_Fullname_Select(String text){
        return "//a[text()='"+text+"']";
    }
    
    public static String Audit_Team_Role(){
        return "//div[@id='control_9D9BAC87-D7A1-4552-A9B0-08F2ACCC4157']/div[1]/div/input";
    }
    
    public static String Audit_Team_sdate(){
        return "//div[@id='control_B1A6F86B-56C1-4100-81F6-F9DE01644413']//input";
    }
    
    public static String Audit_Team_edate(){
        return "//div[@id='control_0957E548-2540-4095-98A0-A8C30DB9FB72']//input";
    }
    
    public static String Audit_Team_save(){
        return "//div[@id='control_6BAFACB3-7AB5-40DA-A504-FFBA016D0981']//div[text()='Save']";
    }
    public static String processFlow() {
        return "//div[@id='btnProcessFlow_form_C9BF4B41-F36D-48FC-8F3F-FE6BC39DEE07']";
    }
    
    //FR6 View Related Obligations
    public static String Audit_ROI_tab(){
        return "//div[text()='Risk, Obligation & Incidents']";
    }
    
    public static String Audit_ROI_dropdown(){
        return "//span[text()='Related Obligations Linked to Business Unit and Impact type']";
    }
    
    public static String Audit_ROI_select(){
        return "(//div[text()='Drag a column header and drop it here to group by that column'])[3]/../div[3]//tr[1]";
    }
    
    public static String Audit_ROI_wait(){
        return "//div[text()='Related Risk']";
    }
    
    //FR9 Capture Audit Actions
    public static String Audit_Actions_tab(){
        return "//div[text()='Actions']";
    }
    
    public static String Audit_Actions_add(){
        return "//div[@id='control_A14067F4-B0D1-468C-B5B8-D0B7BA136E70']//div[text()='Add']";
    }
    
    public static String Audit_Actions_desc(){
        return "//div[@id='control_1255F613-A69C-476A-8B05-4B87E5CA009F']//textarea";
    }
    
    public static String Audit_Actions_deptresp_dropdown(){
        return "//div[@id='control_34D02E21-7837-484C-844E-BCC8CC077837']//li";
    }
    
    public static String Audit_Actions_deptresp_select(String text){
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'"+text+"')]";
    }
    
    public static String Audit_Actions_respper_dropdown(){
        return "//div[@id='control_7854D003-23E6-4A2E-AF2E-357C965FA684']//li";
    }
    
    public static String Audit_Actions_respper_select(String text){
        return "(//div[contains(@class, 'transition visible')]//a[contains(text(),'"+text+"')])[2]";
    }
    
    public static String Audit_Actions_ddate(){
        return "//div[@id='control_A1A7A250-4916-472D-A6A5-CDA980F5DA52']//input";
    }
    
    public static String Audit_Actions_save(){
        return "//div[@id='btnSave_form_604E6965-D428-4B3F-897B-DDE2004A50E3']//div[text()='Save']";
    }
    
    public static String Audit_Actions_savewait(){
        return "(//div[text()='Action Feedback'])[1]";
    }
    
    public static String Audit_processflow(){
        return "//div[@id='btnProcessFlow_form_C9BF4B41-F36D-48FC-8F3F-FE6BC39DEE07']";
    }

    public static String startAuditButton() {
        return "//div[text()='Start Audit']";
    }
    
    //fr4
    public static String Audit_search(){
        return "//div[text()='Search']";
    }
    public static String Af_tab(){
        return "//div[text()='Findings']";
    }
    public static String af_add(){
        return "(//div[text()='Add'])[2]";
    }
    public static String af_processflow(){
        return "//div[@id='btnProcessFlow_form_CEB65E1E-0FE0-410C-9786-1A7C036B69D0']";
    }
    public static String af_elements_dropdown(){
        return "//div[@id='control_6690F99C-7292-4FAA-8032-1DEF729FA474']";
    }
    public static String af_elements_select(String text){
        return "//a[text()='"+text+"']";
    }
    public static String af_desc(){
        return "//div[@id='control_40ECC722-B08B-48F3-9906-3CFCE527C5CD']/div/div/textarea";
    }
    public static String af_owner_dropdown(){
        return "//div[@id='control_434D9128-5EBC-4E25-9836-72A2C4451733']";
    }
    public static String af_owner_select(String text){
        return "(//a[contains(text(),'"+text+"')])[1]";
    }
    public static String af_risksource_dropdown(){
        return "//div[@id='control_E2CBB706-26AB-4373-813A-05D4860FC38F']";
    }
    public static String af_risksource_select(String text){
        return "(//a[contains(text(),'"+text+"')]/../i)[4]";
    }
    public static String af_risksource_select2(String text){
        return "(//a[contains(text(),'"+text+"')]/i[1])[3]";
    }
    public static String af_risksource_arrow(){
        return "//div[@id='control_E2CBB706-26AB-4373-813A-05D4860FC38F']//b[@class='select3-down drop_click']";
    }
    public static String af_class_dropdown(){
        return "//div[@id='control_7689E71C-DC7E-41C1-9124-C6312596E956']";
    }
    public static String af_class_select (String text){
        return "(//a[contains(text(),'"+text+"')])[1]";
    }
    public static String af_risk_dropdown(){
        return "//div[@id='control_1D7F3DE4-F9C4-4626-80C9-0E600A0A8FF8']";
    }
    public static String af_risk_select(String text){
        return "//a[text()='"+text+"']";
    }
    public static String af_stc(){
        return "(//div[text()='Save to continue'])[2]";
    }

    //Sign Off
    public static String signOff_tab() {
        return "//li[@id='tab_006AFF62-D481-4DC3-8668-042C24DAA78C']//div[contains(text(),'Sign Off')]";
    }

    public static String signOff_addButton() {
       return "//div[@id='control_96B9B81E-CE18-4CFD-8F16-1496E3427BDF']//div[contains(text(),'Add')]";
    }

    public static String signOff_processflow() {
        return "//div[@id='btnProcessFlow_form_A52889B0-7739-4666-BA87-CE3F77BAB495']";
    }

    public static String auditorType_Tab() {
        return "//div[@id='control_F8F35C1B-6B3C-4885-9834-C64C5DDA4555']";
    }

    public static String auditorType(String data) {
        return "//a[text()='"+data+"']";
    }
    
    public static String auditor(String data) {
        return "(//a[contains(text(),'" + data + "')])[2]";
    }

    public static String auditor_Tab() {
        return "//div[@id='control_DEA38CFE-258E-4507-BE92-180D8536EF89']";
    }

    public static String signOff_Tab() {
        return "//div[@id='control_AC075E37-7F8F-4DEC-8DF0-02DC8D70BDFB']";
    }
    
    public static String signOffMain(String data) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'"+data+"')]";
    }
    
    public static String signOff(String data) {
        return "(//div[contains(@class, 'transition visible')]//a[contains(text(),'"+data+"')])[5]";
    }
    public static String signOff2(String data) {
        return "(//a[contains(text(),'" + data + "')])[5]";
    }
    public static String comments() {
        return "//div[@id='control_5FC74CF8-0BE4-459D-9599-A35927F43D28']//textarea";
    }

    public static String emailTo(String data) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'"+data+"')]//i[@class='jstree-icon jstree-checkbox']";
    }

    public static String signOff_saveButton() {
        return "(//div[@id='btnSave_form_A52889B0-7739-4666-BA87-CE3F77BAB495']//div[contains(text(),'Save')])[3]";
    }
    
    public static String audit_findings(){
        return "//div[text()='Findings']";
    }
    
    public static String audit_signoff(){
        return "//div[text()='Submit for sign off']";
    }

    public static String close_AuditSignOffTab() {
        return "(//div[@id='form_A52889B0-7739-4666-BA87-CE3F77BAB495']//i[@class='close icon cross'])[1]";
    }

    public static String addButton() {
        return "(//div[@id='control_2FD21E6C-8939-4BE7-B454-7B622DA3C71B']//div[contains(text(),'Add')])[1]";
    }

    public static String actionsFeedbackTab() {
        return "//li[@id='tab_99358277-8A7C-40A1-9ED3-7613E44800C2']";
    }

    public static String actionFeedback() {
        return "//div[@id='control_B9A4BEF3-B4D1-491D-881A-6021DDE86169']//textarea";
    }

    public static String actionFeedback_ProcessFlow() {
        return "//div[@id='btnProcessFlow_form_70F855C4-38C3-4717-ADA2-27C0F8880E46']";
    }

    public static String actionCompleteTab() {
        return "//div[@id='control_40EFC614-1BC1-4F14-934E-928BF0980FF2']";
    }

    public static String select_actionComplete(String data) {
        return "(//a[contains(text(),'" + data + "')])[2]";
    }

    public static String feedbackTo(String data) {
        return "(//a[contains(text(),'" + data + "')]//i)[5]";
    }

    public static String actionFeedback_saveButton() {
        return "//div[@id='btnSave_form_70F855C4-38C3-4717-ADA2-27C0F8880E46']";
    }

    public static String actions_ProcessFlow() {
        return "//div[@id='btnProcessFlow_form_604E6965-D428-4B3F-897B-DDE2004A50E3']";
    }

    public static String close_AuditFeedbackTab() {
        return "(//div[@id='form_70F855C4-38C3-4717-ADA2-27C0F8880E46']//i[@class='close icon cross'])[1]";
    }

    public static String close_AuditActionTab() {
        return "(//div[@id='form_604E6965-D428-4B3F-897B-DDE2004A50E3']//i[@class='close icon cross'])[1]";
    }

    public static String validate_status() {
        return "//div[@id='control_179D01D9-B34C-4492-8566-CADBDC25C4A3']//li[contains(text(),'Completed')]";
    }
    public static String validate_status_InProgress() {
        return "//div[@id='control_179D01D9-B34C-4492-8566-CADBDC25C4A3']//li[contains(text(),'In progress')]";
    }
    
    public static String audit_reports(){
        return "//div[@id='btnReports']";
    }
    
    public static String view_reports(){
        return "//span[@title='View report ']";
    }

    public static String full_Reports(){
        return "//span[@title='Full report ']";
    }
    
    public static String popup_conf(){
        return "//div[@id='btnConfirmYes']";
    }
    
    public static String iframeXpath(){
        return "//iframe[@id='ifrMain']";
    }
    
    public static String view_wait(){
        return "//div[text()='Audit Report']";
    }
    
    public static String full_wait(){
        return "//div[text()='Business unit']";
    }
    
    public static String proto(){
        return "//div[text()='Audit Protocol']";
    }

    public static String Audit_RIL_dropdown() {
        return "//span[text()='Related Incidents Linked to Business Unit,  Impact type and Process/Activity']";
    }

    public static String Audit_RIL_select() {
        return "(//div[@id='control_49585CC2-5547-456B-987A-208F91A99183']//div[text()='Drag a column header and drop it here to group by that column'])/../div[3]//tr[1]";
    }

    public static String Audit_RIL_wait() {
        return "//div[text()='Incident title']";
    }

    public static String link_Btn()
    {
        return "//b[@class='linkbox-link']";
    }
}
